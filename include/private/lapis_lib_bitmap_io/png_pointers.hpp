
#pragma once

#ifndef LAPIS_LIB_BITMAP_IO_PNG_POINTERS_INCLUDED
#define LAPIS_LIB_BITMAP_IO_PNG_POINTERS_INCLUDED 1

#include <utility>

#include <png.h>

namespace lapis::libbitmapio
{
struct PNGPointers
{
    enum class Mode
    {
        READ,
        WRITE
    };

    png_structp pngStruct = nullptr;
    png_infop infoStruct = nullptr;
    Mode mode = Mode::READ;

    // constructors and destructors
    PNGPointers() = default;
    PNGPointers(const PNGPointers&) = delete;
    constexpr PNGPointers(PNGPointers&& other) noexcept:
        pngStruct(std::exchange(other.pngStruct, nullptr)),
        infoStruct(std::exchange(other.infoStruct, nullptr)),
        mode(other.mode)
    {}

    inline ~PNGPointers() noexcept
    {
        deleteStructs();
    }

    // assignment
    PNGPointers& operator=(const PNGPointers&) = delete;
    inline PNGPointers& operator=(PNGPointers&& other) noexcept
    {
        deleteStructs(); // probably not required, but for completeness sake

        pngStruct = std::exchange(other.pngStruct, nullptr);
        infoStruct = std::exchange(other.infoStruct, nullptr);
        mode = other.mode;
        return *this;
    }

private: //utility
    inline void deleteStructs()
    {
        if (pngStruct != nullptr)
        {
            if (mode == Mode::READ)
            {
                png_destroy_read_struct(
                    &pngStruct,
                    infoStruct == nullptr ? nullptr : &infoStruct,
                    nullptr
                );
            }
            else
            {
                png_destroy_write_struct(
                    &pngStruct,
                    infoStruct == nullptr ? nullptr : &infoStruct
                );
            }
        }
    }
};
}

#endif // LAPIS_LIB_CORE_PNG_POINTERS_INCLUDED
