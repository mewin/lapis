
#pragma once

#ifndef LAPIS_LIB_BITMAP_IO_ADAPTER_BITMAP_PNG_HPP_INCLUDED
#define LAPIS_LIB_BITMAP_IO_ADAPTER_BITMAP_PNG_HPP_INCLUDED 1

#include "lapis/adapter.hpp"
#include "lapis/types/bitmap.hpp"

namespace lapis::libbitmapio
{
class AdapterBitmapPNG : public Adapter
{
public:
    AdapterBitmapPNG();
    
public: // implementation
    bool canHandle(const Context& context, const std::string& fileName) const noexcept override;
    ObjectPtr<> fromBlob(const Context& context, const Blob& blob, const std::string& fileName) override;
    Blob toBlob(const Context& context, const Object& object, const std::string& fileName) override;
private:
    [[nodiscard]]
    bool checkFormatCompatible(const types::PixelFormat& format) const noexcept;
};
}

#endif // LAPIS_LIB_BITMAP_IO_ADAPTER_BITMAP_PNG_HPP_INCLUDED
