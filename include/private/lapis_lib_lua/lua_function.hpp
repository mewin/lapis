
#pragma once

#ifndef LAPIS_LIB_LUA_LUA_FUNCTION_HPP_INCLUDED
#define LAPIS_LIB_LUA_LUA_FUNCTION_HPP_INCLUDED 1

#include "lapis/function.hpp"
#include "lapis/value.hpp"
#include "lapis_lib_lua/lua_script.hpp"

namespace lapis::liblua
{
typedef struct lua_State lua_State;
class LuaFunction : public Function
{
private:
    lua_State* state_;
    std::size_t functionIndex_;
public:
    LuaFunction(lua_State* state, int stackIdx);
public:
    constexpr void invalidate() noexcept { functionIndex_ = 0; }
public: // implementation
    Value invoke(const std::vector<Value>& arguments) override;

public:
    [[nodiscard]]
    static inline FunctionPtr wrap(lua_State* state, int stackIdx)
    {
        return std::make_shared<LuaFunction>(state, stackIdx);
    }
};
}

#endif // LAPIS_LIB_LUA_LUA_FUNCTION_HPP_INCLUDED
