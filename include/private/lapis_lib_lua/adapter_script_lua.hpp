
#pragma once

#ifndef LAPIS_LIB_LUA_ADAPTER_SCRIPT_LUA_HPP_INCLUDED
#define LAPIS_LIB_LUA_ADAPTER_SCRIPT_LUA_HPP_INCLUDED 1

#include "lapis/adapter.hpp"

namespace lapis::liblua
{
class AdapterScriptLua : public Adapter
{
public:
    AdapterScriptLua();

public: // implementation
    bool canHandle(const Context& context, const std::string& fileName) const noexcept override;
    ObjectPtr<> fromBlob(const Context& context, const Blob& blob, const std::string& fileName) override;
};
}

#endif // LAPIS_LIB_LUA_ADAPTER_SCRIPT_LUA_HPP_INCLUDED
