
#pragma once

#ifndef LAPIS_LIB_LUA_LUA_STATE_HPP_INCLUDED
#define LAPIS_LIB_LUA_LUA_STATE_HPP_INCLUDED 1

#include <utility>
#include <stdexcept>
#include <string>

#include "lapis/script.hpp"
#include "lapis/value.hpp"

typedef struct lua_State lua_State;
namespace lapis::liblua
{
class LuaScript : public Script
{
private:
    lua_State* handle_ = nullptr;
public:
    LuaScript();
    LuaScript(const LuaScript&) = delete;
    constexpr LuaScript(LuaScript&& other) noexcept
        : handle_(std::exchange(other.handle_, nullptr)) {}
    ~LuaScript();
public:
    LuaScript& operator=(const LuaScript&) = delete;
    constexpr LuaScript& operator=(LuaScript&& other) noexcept
    {
        handle_ = std::exchange(other.handle_, nullptr);
        return *this;
    }
    constexpr bool operator !() const noexcept {
        return handle_ == nullptr;
    }
public:
    constexpr operator bool() const noexcept {
        return handle_ != nullptr;
    }
public: // getters
    [[nodiscard]]
    constexpr lua_State* handle() const noexcept { return handle_; }
public: // loading
    void loadString(const std::string& code);
    void loadBlob(const Blob& code);
public: // Script implementation
    std::vector<Value> execute(const std::vector<Value>& arguments = {}) override;
    void registerObjectClass(const ObjectClass& objectClass) override;
    void setGlobal(const std::string& name, const Value& value) override;
    Value getGlobal(const std::string& name) override;
private:
    inline void checkHandle() const;
    inline void checkError(int err) const;
};
}

#endif // LAPIS_LIB_LUA_LUA_STATE_HPP_INCLUDED
