
#pragma once

#ifndef LAPIS_LIB_LUA_LUA_EXCEPTION_HPP_INCLUDED
#define LAPIS_LIB_LUA_LUA_EXCEPTION_HPP_INCLUDED 1

#include <stdexcept>

typedef struct lua_State lua_State;
namespace lapis::liblua
{
class LuaException : public std::runtime_error
{
public:
    explicit LuaException(const std::string& description) noexcept
         : std::runtime_error("Lua error: " + description) {}
    LuaException(lua_State* state, int error);
};
}

#endif // LAPIS_LIB_LUA_LUA_EXCEPTION_HPP_INCLUDED
