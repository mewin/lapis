
#pragma once

#ifndef LAPIS_LIB_LUA_LUA_UTIL_HPP_INCLUDED
#define LAPIS_LIB_LUA_LUA_UTIL_HPP_INCLUDED 1

#include <vector>

#include <lua.hpp>

#include "lapis/function.hpp"
#include "lapis/value.hpp"
#include "./lua_exception.hpp"

namespace lapis::liblua
{
int normalizeIndex(lua_State* state, int index) noexcept;
void* newAlignedUserdata(lua_State* state, std::size_t size, std::size_t alignment);

template<typename T, typename... TArgs>
T& createUserObject(lua_State* state, TArgs&&... args)
{
    T* object = static_cast<T*>(newAlignedUserdata(state, sizeof(T), alignof(T)));
    ::new (object) T(std::forward<TArgs>(args)...);
    return *object;
}

inline void checkLuaError(lua_State* state, int err)
{
    if (err != LUA_OK)
    {
        throw LuaException(state, err);
    }
}


void pushAllValues(lua_State* state, const Vector& values);
void pushValueVector(lua_State* state, const Vector& values);
void pushValueMap(lua_State* state, const Map& values);
void pushValueObject(lua_State* state, const ObjectPtr<>& value);
void pushFunction(lua_State* state, const FunctionPtr& value);
void pushValue(lua_State* state, const Value& value);

Vector getValues(lua_State* state, int idxFirst, int idxLast);
Value getValue(lua_State* state, int idx);
Value convertTable(lua_State* state, int idx);
}

#endif // LAPIS_LIB_LUA_LUA_UTIL_HPP_INCLUDED
