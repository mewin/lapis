
#pragma once

#ifndef LAPIS_LIB_CORE_TOOLS_BITMAP_IO_INCLUDED
#define LAPIS_LIB_CORE_TOOLS_BITMAP_IO_INCLUDED 1

#include "lapis/tool.hpp"
#include "lapis/types/bitmap.hpp"

namespace lapis::libcore
{
class ToolBitmapFromBlob : public Tool
{
public:
    static const constexpr auto NAME = "lapis.core.bitmap_from_blob";
    static const constexpr auto INPUT_DATA = "data";
    static const constexpr auto OUTPUT_BITMAP = "bitmap";
public:
    ToolBitmapFromBlob() : Tool(NAME,
    { // parameters
        { .name = INPUT_DATA, .type = ValueType::BLOB }
    },
    { // results
        { .name = OUTPUT_BITMAP, .type = ValueType::OBJECT, .objectClass = &types::Bitmap::CLASS }
    }) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};

class ToolBitmapToBlob : public Tool
{
public:
    static const constexpr auto NAME = "lapis.core.bitmap_to_blob";
    static const constexpr auto INPUT_BITMAP = "bitmap";
    static const constexpr auto INPUT_FILENAME = "filename";
    static const constexpr auto OUTPUT_DATA = "data";
public:
    ToolBitmapToBlob() : Tool(NAME,
    { // parameters
        { .name = INPUT_BITMAP, .type = ValueType::OBJECT, .objectClass = &types::Bitmap::CLASS },
        { .name = INPUT_FILENAME, .type = ValueType::STRING }
    },
    { // results
        { .name = OUTPUT_DATA, .type = ValueType::BLOB }
    }) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};
}

#endif // LAPIS_LIB_CORE_TOOLS_BITMAP_IO_INCLUDED
