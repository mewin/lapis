
#pragma once

#ifndef LAPIS_LIB_CORE_TOOLS_FILE_IO_HPP_INCLUDED
#define LAPIS_LIB_CORE_TOOLS_FILE_IO_HPP_INCLUDED 1

#include "lapis/tool.hpp"

namespace lapis::libcore
{
class ToolReadTextFile : public Tool
{
public:
    static const constexpr auto NAME = "lapis.core.read_text";
    static const constexpr auto INPUT_PATH = "path";
    static const constexpr auto OUTPUT_TEXT = "text";
public:
    inline ToolReadTextFile() : Tool(NAME,
    { // parameters
        { .name = INPUT_PATH, .type = ValueType::STRING }
    },
    { // results
        { .name = OUTPUT_TEXT, .type = ValueType::STRING }
    }
    ) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};

class ToolWriteTextFile : public Tool
{
public:
    static const constexpr auto NAME = "lapis.core.write_text";
    static const constexpr auto INPUT_PATH = "path";
    static const constexpr auto INPUT_TEXT = "text";
public:
    inline ToolWriteTextFile() : Tool(NAME,
    { // parameters
        { .name = INPUT_PATH, .type = ValueType::STRING },
        { .name = INPUT_TEXT, .type = ValueType::STRING }
    },
    { // results

    }) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};

class ToolReadBinaryFile : public Tool
{
public:
    static const constexpr auto NAME = "lapis.core.read_binary";
    static const constexpr auto INPUT_PATH = "path";
    static const constexpr auto OUTPUT_DATA = "data";
public:
    inline ToolReadBinaryFile() : Tool(NAME,
    { // parameters
        { .name = INPUT_PATH, .type = ValueType::STRING }
    },
    { // results
        { .name = OUTPUT_DATA, .type = ValueType::BLOB }
    }
    ) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};

class ToolWriteBinaryFile : public Tool
{
public:
    static const constexpr auto NAME = "lapis.core.write_binary";
    static const constexpr auto INPUT_PATH = "path";
    static const constexpr auto INPUT_DATA = "data";
public:
    inline ToolWriteBinaryFile() : Tool(NAME,
    { // parameters
        { .name = INPUT_PATH, .type = ValueType::STRING },
        { .name = INPUT_DATA, .type = ValueType::BLOB }
    },
    { // results

    }) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};
}

#endif // LAPIS_LIB_CORE_TOOLS_FILE_IO_HPP_INCLUDED
