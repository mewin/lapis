
#pragma once

#ifndef LAPIS_LIB_CORE_TOOL_ARGUMENTS_HPP_INCLUDED
#define LAPIS_LIB_CORE_TOOL_ARGUMENTS_HPP_INCLUDED 1

#include "lapis/tool.hpp"

namespace lapis::libcore
{
class ToolPipelineArgument : public Tool
{
public:
    static const constexpr auto NAME = "lapis.core.pipeline_argument";
    static const constexpr auto INPUT_NAME = "name";
    static const constexpr auto OUTPUT_VALUE = "value";
public:
    inline ToolPipelineArgument() : Tool(NAME,
    { // parameters
        { .name = INPUT_NAME, .type = ValueType::STRING }
    },
    { // results
        { .name = OUTPUT_VALUE, .type = ValueType::STRING }
    }
    ) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};

class ToolPipelineArguments : public Tool
{
public:
    static const constexpr auto NAME = "lapis.core.pipeline_arguments";
    static const constexpr auto OUTPUT_VALUES = "values";
public:
    inline ToolPipelineArguments() : Tool(NAME,
    { // parameters
        
    },
    { // results
        { .name = OUTPUT_VALUES, .type = ValueType::MAP }
    }
    ) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};
}

#endif // LAPIS_LIB_CORE_TOOL_ARGUMENTS_HPP_INCLUDED
