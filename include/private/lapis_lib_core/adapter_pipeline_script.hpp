
#pragma once

#ifndef LAPIS_LIB_CORE_ADAPTER_PIPELINE_SCRIPT_HPP_INCLUDED
#define LAPIS_LIB_CORE_ADAPTER_PIPELINE_SCRIPT_HPP_INCLUDED 1

#include "lapis/adapter.hpp"
#include "lapis/context.hpp"
#include "lapis/script.hpp"

namespace lapis::libcore
{
class AdapterPipelineScript : public Adapter
{
public:
    AdapterPipelineScript();
public:
    bool canHandle(const Context& context, const std::string& fileName) const noexcept override;
    ObjectPtr<> fromBlob(const Context& context, const Blob& blob, const std::string& fileName) override;
};
}

#endif // LAPIS_LIB_CORE_ADAPTER_PIPELINE_SCRIPT_HPP_INCLUDED
