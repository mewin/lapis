
#pragma once

#ifndef LAPIS_CMD_OPTIONS_HPP_INCLUDED
#define LAPIS_CMD_OPTIONS_HPP_INCLUDED 1

#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

namespace lapis::cmd
{
enum class OperationMode
{
    NONE,
    RUN,
    CONVERT,
    HELP,
    VERSION
};

struct OperationModeRunArguments
{
    std::string fileName;
    std::unordered_map<std::string, std::string> pipelineArguments;
};

struct OperationModeConvertArguments
{
    std::string fileNameFrom;
    std::string fileNameTo;
};

using operation_mode_arguments_t = std::variant<
    OperationModeRunArguments,
    OperationModeConvertArguments
>;

struct ApplicationOptions
{
    OperationMode operationMode = OperationMode::NONE;
    operation_mode_arguments_t operationModeOptions;
    bool verbose = false;
};

void parseCommandLine(int argc, char* argv[], ApplicationOptions& optionsOut);
void printHelp() noexcept;
void printUsage() noexcept;
void printVersion() noexcept;
}

#endif // LAPIS_CMD_OPTIONS_HPP_INCLUDED
