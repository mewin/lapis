
#pragma once

#ifndef LAPIS_CMD_APPLICATION_HPP_INCLUDED
#define LAPIS_CMD_APPLICATION_HPP_INCLUDED 1

#include <stdexcept>

#include "lapis/context.hpp"
#include "./options.hpp"

namespace lapis::cmd
{
class Application
{
private:
    Context context;
public:
    void run(const ApplicationOptions& options);
private:
    void checkOptions(const ApplicationOptions& options);
    void runModeRun(const OperationModeRunArguments& args);
    void runModeConvert(const OperationModeConvertArguments& args);
};

class InvalidOptionsException : public std::runtime_error
{
public:
    inline InvalidOptionsException(const std::string& description)
        : std::runtime_error("Invalid options passed to application: " + description) {}
};
}

#endif // LAPIS_CMD_APPLICATION_HPP_INCLUDED
