
#pragma once

#ifndef LAPIS_STATIC_LIBRARIES_HPP_INCLUDED
#define LAPIS_STATIC_LIBRARIES_HPP_INCLUDED 1

#include <vector>

namespace lapis
{
class Library;

std::vector<Library*>& getStaticLibraries();
void registerStaticLibrary(Library* library);
}

#endif // LAPIS_STATIC_LIBRARIES_HPP_INCLUDED
