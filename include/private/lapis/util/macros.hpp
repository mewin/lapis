
// macros only for internal usage

#pragma once

#ifndef LAPIS_UTIL_MACROS_HPP_INCLUDED
#define LAPIS_UTIL_MACROS_HPP_INCLUDED 1

#include <utility>

namespace lapis
{
template<typename Fn>
class ScopeExit
{
private:
    Fn function_;
public:
    inline ScopeExit(Fn&& function) : function_(std::forward<Fn>(function)) {}
    ScopeExit(const ScopeExit<Fn>&) = delete;
    ScopeExit(ScopeExit<Fn>&&) = default;
    inline ~ScopeExit() { function_(); }
public:
    ScopeExit& operator=(const ScopeExit<Fn>&) = delete;
    ScopeExit& operator=(ScopeExit<Fn>&&) = delete;
};
}

#define LAPIS_COMBINE1(x, y) x ## y
#define LAPIS_COMBINE(x, y) LAPIS_COMBINE1(x, y)

#define LAPIS_ON_SCOPE_EXIT [[gnu::unused]] auto LAPIS_COMBINE(scopeExit, __LINE__) = [&]()

#if LAPIS_LINK_STATIC
#define LAPIS_LIBRARY_ENTRY_POINT(lib_name) \
    lapis::Library* initLibrary_ ## lib_name ()
#else
#define LAPIS_LIBRARY_ENTRY_POINT(lib_name) \
    extern "C" lapis::Library* initLibrary()
#endif

#endif // LAPIS_UTIL_MACROS_HPP_INCLUDED
