
#pragma once

#ifndef LAPIS_LIB_JSON_TOOLS_JSON_HPP_INCLUDED
#define LAPIS_LIB_JSON_TOOLS_JSON_HPP_INCLUDED 1

#include "lapis/tool.hpp"

namespace lapis::libjson
{
class ToolParseJson : public Tool
{
public:
    static const constexpr auto NAME = "lapis.json.parse_json";
    static const constexpr auto INPUT_JSON = "json";
    static const constexpr auto OUTPUT_VALUE = "value";
public:
    inline ToolParseJson() : Tool(NAME,
    { // parameters
        { .name = INPUT_JSON, .type = ValueType::STRING }
    },
    { // results
        { .name = OUTPUT_VALUE, .type = ValueType::ANY }
    }
    ) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};

class ToolGenerateJson : public Tool
{
public:
    static const constexpr auto NAME = "lapis.json.generate_json";
    static const constexpr auto INPUT_VALUE = "value";
    static const constexpr auto OUTPUT_JSON = "json";
public:
    inline ToolGenerateJson() : Tool(NAME,
    { // parameters
        { .name = INPUT_VALUE, .type = ValueType::ANY }
    },
    { // results
        { .name = OUTPUT_JSON, .type = ValueType::STRING }
    }
    ) {}
public:
    void execute(Context& context, const ToolInput& input, ToolOutput& output) override;
};
}

#endif // LAPIS_LIB_JSON_TOOLS_JSON_HPP_INCLUDED
