
#pragma once

#ifndef LAPIS_LIB_JSON_VALUE_JSON_HPP_INCLUDED
#define LAPIS_LIB_JSON_VALUE_JSON_HPP_INCLUDED 1

#include "nlohmann/json.hpp"

#include "lapis/value.hpp"

namespace lapis
{
/*!
 * Converts a Value to json.
 * 
 * \throw std::runtime_error If the value cannot be serialized.
 */
void to_json(nlohmann::json& json, const Value& value);
/*!
 * Converts json to a Value.
 * 
 * \throw std::runtime_error If the value cannot be deserialized.
 */
void from_json(const nlohmann::json& json, Value& value);
}

#endif // LAPIS_LIB_JSON_VALUE_JSON_HPP_INCLUDED
