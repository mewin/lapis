
#pragma once

#ifndef LAPIS_LIB_JSON_ADAPTER_PIPELINE_JSON_HPP_INCLUDED
#define LAPIS_LIB_JSON_ADAPTER_PIPELINE_JSON_HPP_INCLUDED 1

#include "lapis/adapter.hpp"

namespace lapis::libjson
{
class AdapterPipelineJson : public Adapter
{
public:
    AdapterPipelineJson();
public:
    bool canHandle(const Context& context, const std::string& fileName) const noexcept override;
    ObjectPtr<> fromBlob(const Context& context, const Blob& blob, const std::string& fileName) override;
    Blob toBlob(const Context& context, const Object& object, const std::string& fileName) override;
};
}

#endif // LAPIS_LIB_JSON_ADAPTER_PIPELINE_JSON_HPP_INCLUDED
