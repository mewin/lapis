
#pragma once

#ifndef LAPIS_RESOURCE_HPP_INCLUDED
#define LAPIS_RESOURCE_HPP_INCLUDED 1

#include "./object.hpp"

namespace lapis
{

/*!
 * Resource base class.
 * 
 * Any complex resource managed by lapis (images, scenes, videos, ...) is based
 * on this type.
 * 
 * Any resource must support serialization, deserialization and cloning. Also a
 * default (=empty) resource must be provided to construct new instances from.
 * 
 * When there are different representations for the same data (e.g. multiple
 * libs implementing bitmaps in different ways internally), the supposed way
 * to represent this is not subclassing, but instead provide distinct classes
 * and a converter function (see the Converter class).
 */
class Resource : public Object
{
public:
    Resource() = default;
    Resource(const Resource&) = delete;
    Resource(Resource&&) = delete;
public: // operators
    Resource& operator=(const Resource&) = delete;
    Resource& operator=(Resource&&) = delete;
public: // serialization interface
    /*!
     * Serialize the object for storing it in some way. The serialized value
     * may contain anything but new objects.
     * 
     * This data should be used to reconstruct the value of the object using
     * the deserialize() function.
     * 
     * \throw SerializationException If the value can not be serialized.
     */
    virtual Value serialize() const = 0;
    /*!
     * Deserialize the object from the provided data. 
     * 
     * This function should reconstruct (as good as possible) the state of the
     * object when serialize() has been called.
     * 
     * \throw SerializationException If the value can not be deserialized from
     *        the provided data.
     */
    virtual void deserialize(const Value& value) = 0;
};

/*!
 * Exception that is thrown during Object::serialize() and Object::serialize()
 * if anything goes wrong.
 */
class SerializationException : public std::runtime_error
{
public:
    inline explicit SerializationException(const std::string& description)
        : std::runtime_error("Exception during during serialization: " + description) {}
};
}

#endif // LAPIS_RESOURCE_HPP_INCLUDED
