
#pragma once

#ifndef LAPIS_OBJECT_CLASS_HPP_INCLUDED
#define LAPIS_OBJECT_CLASS_HPP_INCLUDED 1

#include <initializer_list>

#include "./function.hpp"
#include "./util/flags.hpp"

namespace lapis
{
class Object;

enum class MemberFunctionFlagsBits
{
    STATIC = 0
};

struct MemberFunctionFlags : FlagsBase<MemberFunctionFlagsBits>
{
    MemberFunctionFlags() = default;
    MemberFunctionFlags(const MemberFunctionFlags&) = default;


    /*!
     * Construct from list of flags.
     */
    constexpr MemberFunctionFlags(std::initializer_list<MemberFunctionFlagsBits> flags) noexcept
        : flags_t(flags) {}

    // operators
    MemberFunctionFlags& operator=(const MemberFunctionFlags&) = default;

    // flag access
    /*!
     * Check if this function is static (i.e. does not need an instance to be invoked).
     */
    [[nodiscard]]
    constexpr bool _static() const noexcept
    {
        return isFlag(MemberFunctionFlagsBits::STATIC);
    }

    /*!
     * Set if this function is static (i.e. does not need an instance to be invoked).
     */
    constexpr MemberFunctionFlags& _static(bool value) noexcept
    {
        setFlag(MemberFunctionFlagsBits::STATIC, value);
        return *this;
    }
};

struct MemberFunction
{
    std::string name;
    FunctionPtr function;
    MemberFunctionFlags flags;
};

struct MemberProperty
{
    std::string name;

};

/*!
 * The class of an object.
 * 
 * Contains information about the class of an object. This is basically the class
 * name/id, a default value that is cloned for constructing a new instance and
 * information about properties and functions used for script interfaces.
 * 
 * You should normally create a static const ObjectClass instance inside your object
 * classes source file and return it from your getClass() implementation.
 * 
 * A class id is normally in the form of <author>.<library>.<class>, e.g.
 * "lapis.core.Bitmap". Ids are used to store objects and should always be unique
 * to your library and not change after your library has been released.
 */
class ObjectClass
{
private:
    std::string id_;
    std::vector<MemberFunction> memberFunctions_;
    std::vector<MemberProperty> memberProperties_;
    const Object* defaultObject_;
    const ObjectClass* baseClass_;
public:
    /*!
     * Constructs a new ObjectClass.
     * 
     * The provided defaultObject is used to construct new instances
     * of the object and should under no circumstances be changed or
     * deleted.
     */
    ObjectClass(const std::string& id, const Object* defaultObject,
        std::vector<MemberFunction> memberFunctions = {},
        std::vector<MemberProperty> memberProperties = {},
        const ObjectClass* baseClass = nullptr);
    ObjectClass(const ObjectClass&) = delete;
    ObjectClass(ObjectClass&&) = default;
public:
    ObjectClass& operator=(const ObjectClass&) = delete;
    ObjectClass& operator=(ObjectClass&&) = default;
    
    [[nodiscard]]
    inline bool operator==(const ObjectClass& other) const noexcept {
        return id_ == other.id_;
    }
public: // getters
    /*!
     * Retrieves the classes id, as provided during construction.
     */
    [[nodiscard]]
    constexpr const std::string& id() const noexcept { return id_; }

    /*!
     * Retrieves this object types member functions.
     */
    [[nodiscard]]
    constexpr const std::vector<MemberFunction>& memberFunctions() const noexcept
        { return memberFunctions_; }
    
    /*!
     * Retrieves this object types member properties.
     */
    [[nodiscard]]
    constexpr const std::vector<MemberProperty>& memberProperties() const noexcept
        { return memberProperties_; }
    
    /*!
     * Retrieves the classes base, if it exists.
     * 
     * Returns nullptr if the class does not have a base.
     */
    [[nodiscard]]
    const ObjectClass* baseClass() const noexcept { return baseClass_; }
public: // functionality
    /*!
     * Constructs a new instance of the object by cloning the default
     * object instance passed during construction.
     */
    Object* newInstance() const;
};
} // namespace lapis
#endif
