
#pragma once

#ifndef LAPIS_COMPATIBILITY_HPP_INCLUDED
#define LAPIS_COMPATIBILITY_HPP_INCLUDED 1

#ifdef LAPIS_NO_DLL
#define LAPIS_DLL
#elif defined(_MSC_VER)
#ifdef LAPIS_PRIVATE
#define LAPIS_DLL __declspec(dllexport)
#else
#define LAPIS_DLL __declspec(dllimport)
#endif
#else // _MSC_VER
#define LAPIS_DLL
#endif // _MSC_VER
#endif // LAPIS_COMPATIBILITY_HPP_INCLUDED
