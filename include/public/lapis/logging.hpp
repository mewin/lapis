
#pragma once

#ifndef LAPIS_LOGGING_HPP_INCLUDED
#define LAPIS_LOGGING_HPP_INCLUDED 1

#include <functional>
#include <string>

#include <fmt/format.h>

namespace lapis
{
enum class LogLevel
{
    DEBUG   = 0,
    VERBOSE = 1,
    INFO    = 2,
    WARNING = 3,
    SEVERE  = 4
};
using logging_function_t = std::function<void(LogLevel, std::string)>;

void defaultLoggingFunction(LogLevel level, const std::string& message) noexcept;

class LoggerMixin
{
protected:
    logging_function_t loggingFunction_ = &defaultLoggingFunction;
    LogLevel minLogLevel_ = LogLevel::INFO;
public:
    LoggerMixin() noexcept;
public:
    void log(LogLevel level, const std::string& message) const noexcept;

    template<typename TFirst, typename... TArgs>
    inline void log(LogLevel level, const std::string& format, TFirst&& first, TArgs&&... args) const noexcept
    {
        std::string message = fmt::format(format, std::forward<TFirst>(first), std::forward<TArgs>(args)...);
        log(level, message);
    }

    template<typename... TArgs>
    inline void debug(const std::string& format, TArgs&&... args) const noexcept
    {
        log(LogLevel::DEBUG, format, std::forward<TArgs>(args)...);
    }

    template<typename... TArgs>
    inline void verbose(const std::string& format, TArgs&&... args) const noexcept
    {
        log(LogLevel::VERBOSE, format, std::forward<TArgs>(args)...);
    }

    template<typename... TArgs>
    inline void info(const std::string& format, TArgs&&... args) const noexcept
    {
        log(LogLevel::INFO, format, std::forward<TArgs>(args)...);
    }

    template<typename... TArgs>
    inline void warning(const std::string& format, TArgs&&... args) const noexcept
    {
        log(LogLevel::WARNING, format, std::forward<TArgs>(args)...);
    }

    template<typename... TArgs>
    inline void severe(const std::string& format, TArgs&&... args) const noexcept
    {
        log(LogLevel::SEVERE, format, std::forward<TArgs>(args)...);
    }
};

inline std::string logLevelPrefix(LogLevel level) noexcept
{
    switch (level)
    {
    case LogLevel::DEBUG:
        return "D";
    case LogLevel::VERBOSE:
        return "V";
    case LogLevel::INFO:
        return "I";
    case LogLevel::WARNING:
        return "W";
    case LogLevel::SEVERE:
        return "S";
    }
    return "";
}
}

#endif // LAPIS_LOGGING_HPP_INCLUDED
