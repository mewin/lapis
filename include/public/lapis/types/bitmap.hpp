
#pragma once

#ifndef LAPIS_TYPES_BITMAP_HPP_INCLUDED
#define LAPIS_TYPES_BITMAP_HPP_INCLUDED 1

#include <cstdint>
#include <span>
#include <vector>

#include "../compatibility.hpp"
#include "../resource.hpp"

namespace lapis::types
{
enum class PixelDataType : std::uint8_t
{
    FLOAT            = 1,
    INTEGER          = 2,
    UNSIGNED_INTEGER = 3
};

/*!
 * A pixel format description.
 * 
 * Describes the layout of the pixels of a Bitmap object.
 * 
 * Defaults to a 32bit RGBA image.
 */
struct PixelFormat
{
    /*!
     * The data type of the pixels.
     */
    PixelDataType dataType = PixelDataType::UNSIGNED_INTEGER;
    std::uint8_t bytesPerChannel = 1;
    std::uint8_t channels = 4;

    inline std::uint8_t bytesPerPixel() const { return bytesPerChannel * channels; }
};

/*!
 * A (2D) bitmap.
 * 
 * Objects of this type are used to store two-dimensional bitmaps of pixels.
 * This class supports different pixel formats (see the PixelFormat struct and
 * validatePixelFormat() function for more information).
 * 
 * The expected order of channels (if provided) is red, green, blue, alpha.
 */
class Bitmap : public Resource
{
public:
    static const constexpr auto CLASS_ID = "lapis.core.Bitmap";
    LAPIS_DLL static const ObjectClass CLASS;
private:
    Blob data_;
    PixelFormat pixelFormat_;
    unsigned width_ = 0;
public: // getters
    /*!
     * Retrieves the actual pixel data, stored in a continous array.
     * The size of the array is always equal to
     *  `width * height * pixelFormat.channels * pixelFormat.bytesPerChannel`.
     * Interpreting the data is left to the caller.
     */
    inline BlobView data()
        { return {data_.begin(), data_.end()}; }
    inline ConstBlobView data() const
        { return {data_.begin(), data_.end()}; }
    /*!
     * Retrieves the pixel format of this bitmap.
     */
    inline PixelFormat pixelFormat() const { return pixelFormat_; }
    /*!
     * Retrieves the width (in pixels) of this bitmap.
     */
    inline unsigned width() const { return width_; }
    /*!
     * Retrieves the height (in pixels) of this bitmap.
     */
    inline unsigned height() const {
        return static_cast<unsigned>(data_.size() / width_ / pixelFormat_.bytesPerPixel());
    }
    /*!
     * Retrieves the number of bytes per pixel row.
     */
    inline unsigned bytesPerRow() const {
        return width() * pixelFormat_.bytesPerPixel();
    }
public: // data
    /*!
     * Creates a bitmap using the provided width, height and pixel format.
     */
    void create(unsigned width, unsigned height, PixelFormat pixelFormat = {});
public: // Object implementation
    Value serialize() const override;
    void deserialize(const Value& value) override;
    Object* clone() const override;
    const ObjectClass& getClass() const noexcept override;
};

/*!
 * Converts a value of the PixelDataType enum to a corresponding string value
 * for serializtion.
 */
std::string pixelDataTypeToString(PixelDataType pixelDataType) noexcept;
/*!
 * Converts a string back to a PixelDataType after it has been converted using
 * pixelDataTypeToString().
 */
PixelDataType pixelDataTypeFromString(const std::string& str);
/*!
 * Checks if a pixel format is valid.
 * 
 * A valid pixel format must:
 *  - have a minimum of 1 and maximum of 4 channels
 *  - use 4 or 8 bytes per channel for float channels
 *  - use 1, 2, 4 or 8 bytes per channel for integer or unsigned integer channels
 */
bool validatePixelFormat(const PixelFormat& pixelFormat) noexcept;
}

#endif // LAPIS_TYPES_BITMAP_HPP_INCLUDED
