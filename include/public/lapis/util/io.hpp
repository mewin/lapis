
#pragma once

#ifndef LAPIS_UTIL_IO_HPP_INCLUDED
#define LAPIS_UTIL_IO_HPP_INCLUDED 1

#include <array>
#include <cstddef>
#include <cstring>
#include <iostream>
#include <string>

#include "../types.hpp"

namespace lapis
{
static const constexpr std::size_t IO_BLOCK_SIZE = 4096;

template<typename TChar>
std::basic_string<TChar> readWholeStream(std::basic_istream<TChar>& stream)
{
    std::basic_string<TChar> result;
    std::array<TChar, IO_BLOCK_SIZE> buffer;
    
    while (stream.read(buffer.data(), buffer.size()))
    {
        result.append(buffer.data(), stream.gcount());
    }

    return result;
}

template<typename TChar>
Blob readWholeStreamBinary(std::basic_istream<TChar>& stream)
{
    Blob result;
    std::array<TChar, IO_BLOCK_SIZE> buffer;
    
    do
    {
        stream.read(buffer.data(), buffer.size());
        
        const std::size_t oldSize = result.size();
        const std::size_t bufferSize = stream.gcount() * sizeof(TChar);
        result.resize(oldSize + bufferSize);
        std::memcpy(&result[oldSize], buffer.data(), bufferSize);
    } while (stream);

    return result;
}
}

#endif // LAPIS_UTIL_IO_HPP_INCLUDED
