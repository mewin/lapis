
#pragma once

#ifndef LAPIS_UTIL_FUNCTION_TRAITS_HPP_INCLUDED
#define LAPIS_UTIL_FUNCTION_TRAITS_HPP_INCLUDED 1

#include <tuple>

namespace lapis::util
{

template<typename TMemberFn>
struct member_function_traits;

template<typename TFuncObj>
struct function_traits : member_function_traits<decltype(&TFuncObj::operator())> {};

template<typename... TArguments>
struct argument_traits
{
    template<int pos>
    using nth = std::tuple_element_t<pos, std::tuple<TArguments...>>;
};

template<typename TReturn, typename... TArgs>
struct function_traits<TReturn (*) (TArgs...)>
{
    using result_type = TReturn;
    using argument_types = argument_traits<TArgs...>;
};

template<typename TReturn, typename TClass, typename... TArgs>
struct member_function_traits<TReturn (TClass::*) (TArgs...)>
{
    using result_type = TReturn;
    using class_type = TClass;
    using argument_types = argument_traits<TArgs...>;
};

template<typename TReturn, typename TClass, typename... TArgs>
struct member_function_traits<TReturn (TClass::*) (TArgs...) const>
{
    using result_type = TReturn;
    using class_type = TClass;
    using argument_types = argument_traits<TArgs...>;
};
}

#endif // LAPIS_UTIL_FUNCTION_TRAITS_HPP_INCLUDED
