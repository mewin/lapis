
#pragma once

#ifndef LAPIS_UTIL_COLLECTIONS_HPP_INCLUDED
#define LAPIS_UTIL_COLLECTIONS_HPP_INCLUDED 1

#include <algorithm>
#include <vector>

namespace lapis
{
template<typename TIterator, typename TType = typename TIterator::value_type::element_type, typename TResult = std::vector<TType*>>
TResult toRawPointers(TIterator begin, TIterator end)
{
    TResult result;
    std::transform(begin, end, std::back_insert_iterator(result), [](const auto& smart)
    {
        return smart.get();
    });
    return result;
}
}

#endif // LAPIS_UTIL_COLLECTIONS_HPP_INCLUDED
