
#pragma once

#ifndef LAPIS_UTIL_GRAPH_HPP_INCLUDED
#define LAPIS_UTIL_GRAPH_HPP_INCLUDED 1

#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <vector>

namespace lapis
{
/*!
 * Templated graph type, used as base for graph algorithms.
 * 
 * This is used to pass data to graph algorithms. It has to implement several
 * methods that are needed for graph algorithms these are (currently):
 * 
 * node_t edgeStart(edge_t edge) const
 * node_t edgeEnd(edge_t edge) const
 * cost_t edgeCost(edge_t edge) const
 * void edgesFrom(node_t node, std::vector<edge_t>& result) const
 * 
 * This is done in order to allow graph algorithms to operate on existing
 * graph-like data witout copying it.
 */
template<typename TTraits>
struct Graph
{

};

template<typename TTraits>
struct GraphRoute
{
    using edge_t = typename TTraits::edge_t;
    using cost_t = typename TTraits::cost_t;

    std::vector<edge_t> edges;
    cost_t cost = cost_t(0);
    bool success = false;
};

template<typename TTraits>
GraphRoute<TTraits> shortestPath(const Graph<TTraits>& graph,
        const typename TTraits::node_t start,
        const typename TTraits::node_t goal)
{
    using node_t = typename TTraits::node_t;
    using edge_t = typename TTraits::edge_t;
    using cost_t = typename TTraits::cost_t;

    struct QueueNode
    {
        node_t node;
        cost_t cost;

        QueueNode() = default;
        QueueNode(const QueueNode&) = default;
        constexpr QueueNode(node_t _node, cost_t _cost) noexcept 
            : node(_node), cost(_cost) {}

        constexpr bool operator<(const QueueNode& other) const noexcept
        {
            // invert to make lower-cost nodes higher priority
            return other.cost < cost;
        }
    };
    struct SelectedEdge
    {
        edge_t edge;
        cost_t totalCost;
    };

    std::priority_queue<QueueNode> nextNodes;
    std::unordered_set<node_t> explored;
    std::unordered_map<node_t, SelectedEdge> selectedEdges;
    std::vector<edge_t> edgesBuffer; // for performance reasons a single vector is reused

    nextNodes.emplace(start, 0);

    while (!nextNodes.empty())
    {
        QueueNode node = nextNodes.top();
        nextNodes.pop();

        if (explored.contains(node.node))
        {
            // might have been added multiple times
            continue;
        }

        // are we done?
        if (node.node == goal)
        {
            break;
        }

        // mark this node as explored
        explored.insert(node.node);

        // check all neighbouring nodes
        edgesBuffer.clear();
        graph.edgesFrom(node.node, edgesBuffer);

        for (const edge_t& edge : edgesBuffer)
        {
            node_t nodeTo = graph.edgeEnd(edge);
            const cost_t totalCost = node.cost + graph.edgeCost(edge);
            auto itSelected = selectedEdges.find(nodeTo);
            
            // is there already a cheaper path
            if (itSelected != selectedEdges.end() && itSelected->second.totalCost <= totalCost)
            {
                continue;
            }
            selectedEdges[nodeTo] = {edge, totalCost};

            // append possible new node to explore
            // note that std::priority_queue does not support updating priorities
            // therefore a node might be added multiple times and will be skipped
            // using the explored set instead
            if (!explored.contains(nodeTo))
            {
                nextNodes.emplace(nodeTo, totalCost);
            }
        }
    }

    // if there is no edge selected for our goal node, we did not reach it
    auto it = selectedEdges.find(goal);
    if (it == selectedEdges.end())
    {
        return {};
    }

    // compose the route
    GraphRoute<TTraits> route;
    route.cost = selectedEdges.at(goal).totalCost;
    route.success = true;

    for (node_t node = goal; node != start; )
    {
        const SelectedEdge selectedEdge = selectedEdges.at(node);
        route.edges.push_back(selectedEdge.edge);
        node = graph.edgeStart(selectedEdge.edge);
    }

    // edges have been appended in reverse order, revert them again
    std::reverse(route.edges.begin(), route.edges.end());
    
    return route;
}
}

#endif // LAPIS_UTIL_GRAPH_HPP_INCLUDED

