
#pragma once

#include <initializer_list>
#include <type_traits>

#ifndef LAPIS_TOOLS_FLAGS_HPP_INCLUDED
#define LAPIS_TOOLS_FLAGS_HPP_INCLUDED 1

/*!
 * Template class for implementing bitflags.
 * 
 * Uses an enum as basis for its values. As the values of the enum are used to
 * shift the bit inside the flag, they should not be too high. The best way is
 * to simply declare them as increasing numbers from 0 upwards.
 * 
 * \tparam TEnum The enumeration used as a base for the flags.
 * \tparam TBase The base type for this enum. Must be big enough to hold
 *         `(1 << max(TEnum))`.
 */
template<typename TEnum, typename TBase = std::underlying_type_t<TEnum>>
struct FlagsBase
{
public:
    /*!
     * Own type, for simplcities sake.
     */
    using flags_t = FlagsBase<TEnum, TBase>;
    /*!
     * The enum type as defined during template instantiation.
     */
    using enum_t = TEnum;
    /*!
     * The base type as defined during template instantiation.
     */
    using base_t = TBase;
protected:
    /*!
     * The current value of the 
     */
    base_t value_ = 0;
protected:
    FlagsBase() = default;
    FlagsBase(const flags_t&) = default;
    /*!
     * Construct from list of flags.
     */
    constexpr FlagsBase(std::initializer_list<enum_t> flags) noexcept
    {
        for (enum_t flag : flags)
        {
            setFlag(flag, true);
        }
    }
public:
    flags_t& operator=(const flags_t&) = default;
public:
    /*!
     * Check whether a flag is set.
     */
    [[nodiscard]]
    constexpr bool isFlag(TEnum flag) const noexcept
    {
        return (value_ & (base_t(1) << static_cast<base_t>(flag))) != 0;
    }
    /*!
     * Check the value of a flag.
     * 
     * Returns reference to this for chaining.
     */
    constexpr flags_t& setFlag(TEnum flag, bool flag_value) noexcept
    {
        if (flag_value)
        {
            value_ |= (base_t(1) << static_cast<base_t>(flag));
        }
        else
        {
            value_ &= ~(base_t(1) << static_cast<base_t>(flag));
        }
        return *this;
    }
};

#endif // LAPIS_TOOLS_FLAGS_HPP_INCLUDED
