
#pragma once

#ifndef LAPIS_UTIL_TYPE_TRAITS_HPP_INCLUDED
#define LAPIS_UTIL_TYPE_TRAITS_HPP_INCLUDED 1

#include <map>
#include <memory>
#include <unordered_map>
#include <vector>

namespace lapis::util
{
/*!
 * Helper template that always evaluates to false.
 * 
 * Required for conditional compilation situations to make static_assert "depend"
 * on a type.
 */
template<class... T> inline constexpr bool always_false_v = false;

template<typename T>
struct is_shared_ptr : std::false_type {};

template<typename T>
struct is_shared_ptr<std::shared_ptr<T>> : std::true_type {};

template<typename T>
inline constexpr bool is_shared_ptr_v = is_shared_ptr<T>::value;

template<typename T>
struct is_vector : std::false_type {};

template<typename T, typename Allocator>
struct is_vector<std::vector<T, Allocator>> : std::true_type {};

template<typename T>
inline constexpr bool is_vector_v = is_vector<T>::value;

template<typename T>
struct is_map : std::false_type {};

template<typename Key, typename T, typename Compare, typename Allocator>
struct is_map<std::map<Key, T, Compare, Allocator>> : std::true_type {};

template<typename T>
inline constexpr bool is_map_v = is_map<T>::value;

template<typename T>
struct is_unordered_map : std::false_type {};

template<typename Key, typename T, typename Compare, typename Allocator>
struct is_unordered_map<std::unordered_map<Key, T, Compare, Allocator>> : std::true_type {};

template<typename T>
inline constexpr bool is_unordered_map_v = is_unordered_map<T>::value;

template<typename T>
inline constexpr bool is_any_map_v = is_map_v<T> || is_unordered_map_v<T>;
}

#endif // LAPIS_UTIL_TYPE_TRAITS_HPP_INCLUDED
