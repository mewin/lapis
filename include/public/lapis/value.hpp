
#pragma once

#ifndef LAPIS_VALUE_HPP_INCLUDED
#define LAPIS_VALUE_HPP_INCLUDED 1

#include <cstdint>
#include <map>
#include <memory>
#include <string>
#include <variant>
#include <vector>

#include "./types.hpp"
#include "./util/type_traits.hpp"

namespace lapis
{
class Object; // cyclic dependency
class Function;
/*!
 * Enumeration of supported value types.
 * 
 * \see Value::type()
 */
enum class ValueType
{
    NONE     = 0,
    ANY      = 1,
    BOOLEAN  = 2,
    INTEGER  = 3,
    DOUBLE   = 4,
    STRING   = 5,
    OBJECT   = 6,
    VECTOR   = 7,
    MAP      = 8,
    BLOB     = 9,
    FUNCTION = 10
};

namespace ValueTypeName
{
static const constexpr auto NONE     = "NONE";
static const constexpr auto ANY      = "ANY";
static const constexpr auto BOOLEAN  = "BOOLEAN";
static const constexpr auto INTEGER  = "INTEGER";
static const constexpr auto DOUBLE   = "DOUBLE";
static const constexpr auto STRING   = "STRING";
static const constexpr auto OBJECT   = "OBJECT";
static const constexpr auto VECTOR   = "VECTOR";
static const constexpr auto MAP      = "MAP";
static const constexpr auto BLOB     = "BLOB";
static const constexpr auto FUNCTION = "FUNCTION";
}

/*!
 * Value type for tool input/output.
 *
 * This is the basic type that is passed between tools.
 */
class Value
{
public:
    static const Value EMPTY; // for functions that return a const reference
public:
    // TODO: custom (unicode) string class
    using store_t = std::variant<
        std::nullptr_t,
        bool,
        int,
        double,
        std::string,
        ObjectPtr<>,
        Vector,
        Map,
        Blob,
        FunctionPtr
    >;
private:
    store_t store_;
public: // default constructors
    Value() = default;
    Value(const Value&) = default;
    Value(Value&&) = default;
public: // value initialization
    inline Value(bool store) : store_(store) {}
    inline Value(int store) : store_(store) {}
    inline Value(double store) : store_(store) {}
    inline Value(const std::string& store) : store_(store) {}
    inline Value(const char* store) : Value(std::string(store)) {}
    inline Value(const Vector& store) : store_(store) {}
    inline Value(Vector&& store) : store_(std::move(store)) {}
    inline Value(const Map& store) : store_(store) {}
    inline Value(Map&& store) : store_(std::move(store)) {}
    inline Value(const Blob& store) : store_(store) {}
    inline Value(Blob&& store) : store_(std::move(store)) {}
    inline Value(const FunctionPtr& store) : store_(store) {}
    inline Value(FunctionPtr&& store) : store_(std::move(store)) {}

    template<typename TObject> requires std::is_base_of_v<Object, TObject>
    inline Value(std::shared_ptr<TObject> store) : store_(std::move(store)) {}
public:
    Value& operator =(const Value&) = default;
    Value& operator =(Value&&) = default;
public:
    /*!
     * Retrieves the type of the stored value.
     */
    ValueType type() const noexcept;
    /*!
     * Checks if this Value is empty (contains no value).
     */
    inline bool empty() const noexcept { return type() == ValueType::NONE; }
    /*!
     * Attempts to convert the value to a boolean.
     *
     * Returns fallback if no conversion is possible.
     */
    bool asBool(bool fallback = false) const noexcept;
    /*!
     * Attempts to convert the value to an integer.
     *
     * Returns fallback if no conversion is possible.
     */
    int asInteger(int fallback = 0) const noexcept;
    /*!
     * Attempts to convert the value to a double.
     *
     * Returns fallback if no conversion is possible.
     */
    double asDouble(double fallback = 0.0) const noexcept;
    /*!
     * Attempts to convert the value to a string.
     *
     * Returns fallback if no conversion is possible.
     */
    std::string asString(const std::string& fallback = "") const noexcept;
    /*!
     * Attempts to convert the value to any subtype of object.
     *
     * Returns nullptr if no conversion is possible.
     */
    template<typename TObject = Object> requires std::is_base_of_v<Object, TObject>
    ObjectPtr<TObject> asObject() const noexcept
    {
        return std::dynamic_pointer_cast<TObject>(asObjectImpl());
    }
    /*!
     * Attempts to convert the value to an object.
     *
     * Returns nullptr if no conversion is possible.
     */
    ObjectPtr<> asObjectImpl() const noexcept;
    /*!
     * Attempts to convert the value to a vector.
     *
     * Returns an empty vector if no conversion is possible.
     * 
     * Note that if the value is converted, it is temporarily stored
     * inside a thread local buffer that may be overwritten as soon
     * as another conversion takes place. Therefore you should copy
     * the result before accessing another value.
     * 
     * \param convert Allows conversion.
     */
    const Vector& asVector(bool convert = false) const noexcept;
    /*!
     * Attempts to convert the value to a map.
     *
     * Returns an empty map if no conversion is possible.
     */
    const Map& asMap() const noexcept;
    /*!
     * Attempts to convert the value to a blob.
     *
     * Returns an empty blob if no conversion is possible.
     */
    const Blob& asBlob() const noexcept;
    /*!
     * Attempts to convert the value to a function.
     *
     * Returns an empty function (nullptr) if no conversion is possible.
     */
    FunctionPtr asFunction() const noexcept;

    /*!
     * Smart conversion to different types.
     * 
     * Supported types are, as of now:
     * 
     * - bool
     * - any integral type
     * - any floating point type
     * - std::string
     * - pointer to any subclass of Object
     * - shared_ptr to any subclass of Object
     * - std::vector of any supported type
     * - std::map and std::unordered_map mapping std::string to any supported type
     */
    template<typename T>
    inline auto as(T fallback = T()) const noexcept;
};

template<typename T>
inline auto Value::as(T fallback) const noexcept
{
    using type_t = std::remove_reference_t<T>;

    if constexpr (std::is_same_v<type_t, Value>)
    {
        return *this;
    }
    else if constexpr (std::is_same_v<type_t, bool>)
    {
        return asBool(fallback);
    }
    else if constexpr (std::is_integral_v<type_t>)
    {
        return static_cast<type_t>(asInteger(static_cast<int>(fallback)));
    }
    else if constexpr (std::is_floating_point_v<T>)
    {
        return static_cast<type_t>(asDouble(static_cast<double>(fallback)));
    }
    else if constexpr (std::is_same_v<type_t, std::string>)
    {
        return asString(fallback);
    }
    else if constexpr (util::is_shared_ptr_v<type_t>)
    {
        static_assert(std::is_base_of_v<Object, typename type_t::element_type>,
            "Cannot cast to this shared_ptr type.");
        return std::dynamic_pointer_cast<typename type_t::element_type>(asObject());
    }
    else if constexpr (std::is_pointer_v<type_t>)
    {
        static_assert(std::is_base_of_v<Object, std::remove_pointer_t<type_t>>,
            "Cannot cast to this pointer type.");
        return dynamic_cast<type_t>(asObject().get());
    }
    else if constexpr (util::is_vector_v<type_t>)
    {
        type_t outVector;
        Vector myVector = asVector(true);
        outVector.reserve(myVector.size());
        for (const Value& value : myVector)
        {
            outVector.emplace_back(value.as<typename type_t::value_type>());
        }
        return outVector;
    }
    else if constexpr (util::is_any_map_v<type_t>)
    {
        static_assert(std::is_same_v<typename type_t::key_type, std::string>,
            "Cannot assign to map with key other than std::string.");
        type_t outMap;
        const Map& myMap = asMap();
        for (const auto& entry : myMap)
        {
            outMap.insert(std::make_pair(entry.first, entry.second.as<typename type_t::mapped_type>()));
        }
        return outMap;
    }
    else
    {
        static_assert(util::always_false_v<type_t>, "Cannot cast value to this type.");
    }
}
}

#endif // LAPIS_VALUE_HPP_INCLUDED
