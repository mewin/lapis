
#pragma once

#ifndef LAPIS_ADAPTER_HPP_INCLUDED
#define LAPIS_ADAPTER_HPP_INCLUDED 1

#include "./object.hpp"
#include "./value.hpp"
#include "./util/flags.hpp"

namespace lapis
{
class Context;
/*!
 * Values for the AdapterFlags type.
 * 
 * \see AdapterFlags
 */
enum class AdapterFlagsBits
{
    CAN_IMPORT              = 0,
    CAN_EXPORT              = 1
};

/*!
 * Specifies the captibilites of an Adapter.
 * 
 * \see Adapter
 */
struct AdapterFlags : FlagsBase<AdapterFlagsBits>
{
    AdapterFlags() = default;
    AdapterFlags(const AdapterFlags&) = default;

    /*!
     * Construct from list of flags.
     */
    constexpr AdapterFlags(std::initializer_list<AdapterFlagsBits> flags) noexcept : flags_t(flags)
    {

    }

    // operators
    AdapterFlags& operator=(const AdapterFlags&) = default;

    // flag access
    /*!
     * Check if this adapter can be used to import resources.
     */
    [[nodiscard]]
    constexpr bool canImport() const noexcept
    {
        return isFlag(AdapterFlagsBits::CAN_IMPORT);
    }

    /*!
     * Set if this adapter can be used to import resources.
     */
    constexpr AdapterFlags& canImport(bool value) noexcept
    {
        setFlag(AdapterFlagsBits::CAN_IMPORT, value);
        return *this;
    }
    
    /*!
     * Check if this adapter can be used to export resources.
     */
    [[nodiscard]]
    constexpr bool canExport() const noexcept
    {
        return isFlag(AdapterFlagsBits::CAN_EXPORT);
    }

    /*!
     * Set if this adapter can be used to export resources.
     */
    constexpr AdapterFlags& canExport(bool value) noexcept
    {
        setFlag(AdapterFlagsBits::CAN_EXPORT, value);
        return *this;
    }
};

/*!
 * An adapter is an object that is used to im-/export resources.
 * 
 * Im-/Export tools rely on adapters to parse files. This allows plugins to
 * implement custom formats without having to declare a seperate tool.
 * 
 * Adapters may be pure in-/output adapters, but must at least support either
 * parsing from blob or converting to blob.
 */
class Adapter : public Object
{
public:
    static const constexpr auto CLASS_ID = "lapis.Adapter";
    static const ObjectClass CLASS;
private:
    const ObjectClass& objectClass_;
    const AdapterFlags flags_;
protected:
    explicit Adapter(const ObjectClass& objectClass, AdapterFlags flags);
    Adapter(const Adapter&) = delete;
    Adapter(Adapter&&) = delete;
public:
    virtual ~Adapter();
public: // operators
    Adapter& operator=(const Adapter&) = delete;
    Adapter& operator=(Adapter&&) = delete;
public: // member access
    /*!
     * Retrieves the class of the objects that are im-/exported using this
     * adapter.
     */
    [[nodiscard]]
    constexpr const ObjectClass& objectClass() const noexcept { return objectClass_; }
    /*!
     * Retrieves the flags of this adapter. Used to check for specific
     * capabilities.
     */
    [[nodiscard]]
    constexpr const AdapterFlags flags() const noexcept { return flags_; }
public: // interface
    /*!
     * Check if a file is supported by this interface. Only uses the file name.
     * 
     * Normally only the extension of the file name is checked here.
     * 
     * \param context  The active context.
     * \param fileName The name of the file (without complete path).
     * 
     * \return true If this interface can handle this type of file.
     */
    [[nodiscard]]
    virtual bool canHandle(const Context& context, const std::string& fileName) const noexcept = 0;
    /*!
     * Allocates and constructs an object from a blob of data.
     * 
     * The ownership of the object is passed on and it is the callers
     * responsibility to delete the object when it is on longer needed.
     * This is normally done by passing the object into a smart pointer.
     * 
     * May return nullptr if the given blob is not supported by this adapter.
     * 
     * Requires AdapterFlags::canImport() to be set.
     */
    [[nodiscard("Allocated object not used.")]]
    virtual ObjectPtr<> fromBlob(const Context& context, const Blob& blob, const std::string& fileName);

    /*!
     * Converts the passed object into a blob of data.
     * 
     * Requires AdapterFlags::canExport() to be set.
     */
    [[nodiscard]]
    virtual Blob toBlob(const Context& context, const Object& object, const std::string& fileName);
public: // Object implementation
    const ObjectClass& getClass() const noexcept override;
};
}

#endif // LAPIS_ADAPTER_HPP_INCLUDED
