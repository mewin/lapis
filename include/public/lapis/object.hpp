
#pragma once

#ifndef LAPIS_OBJECT_HPP_INCLUDED
#define LAPIS_OBJECT_HPP_INCLUDED 1

#include <stdexcept>
#include <string>

#include "./object_class.hpp"
#include "./value.hpp"

namespace lapis
{
/*!
 * Object base class.
 * 
 * Classes that extend Object may be accessable via script. Objects must support
 * cloning in order for dynamic (i.e. script) creation to work.
 */
class Object
{
protected:
    Object() = default;
    Object(const Object&) = default;
    Object(Object&&) = default;
public:
    virtual ~Object();
protected:
    Object& operator=(const Object&) = default;
    Object& operator=(Object&&) = default;
public:
    /*!
     * Convert the object to a readable value. This value is intended for
     * the users eyes and not to be converted back.
     * 
     * By default returns "[<class name>@<address>]".
     */
    virtual std::string toString() const noexcept;
    /*!
     * Clone the object.
     * 
     * This function should create a copy of the object that contains the same
     * data and reacts to function calls in the same way.
     */
    virtual Object* clone() const;
    /*!
     * Retrieves the objects class.
     * 
     * This is normally a static variable stored inside the objects source file.
     */
    virtual const ObjectClass& getClass() const noexcept = 0;
};

/*!
 * Exception that is thrown at the attempt of cloning an object that is not
 * clonable.
 */
class NotClonableException : public std::runtime_error
{
public:
    explicit inline NotClonableException(const std::string& classId) noexcept
        : std::runtime_error("Attempting to clone a not clonable class: " + classId) {}
};
}

#endif // LAPIS_OBJECT_HPP_INCLUDED
