
#pragma once

#ifndef LAPIS_VERSION_HPP_INCLUDED
#define LAPIS_VERSION_HPP_INCLUDED 1

#include <cstdint>
#include <string>

#include "./compatibility.hpp"

namespace lapis
{
/*!
 * A version number.
 * 
 * Contains a SemVer-ish version number (major.minor.patch). Used wherever
 * versioning is used (the main API, libraries, ...).
 */
struct Version
{
/*!
 * The major version.
 * 
 * Increased everytime a backwards compatiblity breaking change is introduced.
 */
std::uint16_t major = 0;
/*!
 * The minor version.
 * 
 * Increased when a backwards compatible feature change is introduced.
 * Reset to 0 whenever major is increased.
 */
std::uint16_t minor = 0;
/*!
 * The patch version.
 * 
 * Increased when a non-feature (i.e. bugfix) change is introduced. Reset to
 * 0 whenever major or minor is increased.
 */
std::uint32_t patch = 0;
};

/*!
 * The version number of the library binary.
 */
LAPIS_DLL extern const Version LIBRARY_VERSION;
/*!
 * The version number of the library binary, converted to a string for convenience.
 * 
 * \see LIBRARY_VERSION
 * \see versionString()
 */
LAPIS_DLL extern const std::string LIBRARY_VERSION_STRING;
/*!
 * Converts a Version struct to a string, seperated by periods.
 * 
 * For example "1.2.34" equals major version 1, minor version 2 and patch
 * version 34.
 */
std::string versionString(const Version& version) noexcept;
}

#endif // LAPIS_VERSION_HPP_INCLUDED
