
#pragma once

#ifndef LAPIS_CONTEXT_HPP_INCLUDED
#define LAPIS_CONTEXT_HPP_INCLUDED 1

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

#include "./adapter.hpp"
#include "./converter.hpp"
#include "./library.hpp"
#include "./logging.hpp"
#include "./object.hpp"
#include "./options.hpp"
#include "./pipeline.hpp"
#include "./tool.hpp"

namespace lapis
{
namespace OptionKeys
{
    static const constexpr auto VERBOSE = "verbose";
}
/*!
 * Manages the state of a pipeline execution.
 * 
 * The context is passed to each tool and stores global state and tool
 * results for further usage.
 */
class Context : public Object, public LoggerMixin
{
public:
    static const constexpr auto CLASS_ID = "lapis.Context";
    static const ObjectClass CLASS;
private:
    Options options_;
    std::unordered_map<std::string, std::unique_ptr<Tool>> tools_;
    std::unordered_map<std::string, std::string> arguments_;
    std::unordered_map<std::string, const ObjectClass*> objectClasses_;
    std::unordered_map<const ObjectClass*, std::vector<std::unique_ptr<Adapter>>> adapters_;
    std::vector<std::unique_ptr<Converter>> converters_;
    std::vector<Library*> libraries_;
public: // getters
    /*!
     */
    [[nodiscard]] inline Options& options() noexcept { return options_; }
    /*!
     */
    [[nodiscard]] inline const Options& options() const noexcept { return options_; }
    /*!
    */
    [[nodiscard]] inline std::unordered_map<std::string, std::string>&
    arguments() noexcept { return arguments_; }
    /*!
     */
    [[nodiscard]] inline const std::unordered_map<std::string, std::string>&
    arguments() const noexcept { return arguments_; }
    /*!
     */
    [[nodiscard]] constexpr const std::unordered_map<std::string, const ObjectClass*>&
    objectClasses() const noexcept { return objectClasses_; }
public: // libraries
    /*!
     * Loads all available libraries.
     * 
     * Libraries are gathered from all default/configured library paths.
     */
    void loadAllLibraries();
public: // import and export
    /*!
     * Import a pipeline from the specified stream.
     * 
     * The filename is used in order to find a fitting adapter for the stream
     * and may be artifical (like "file.json").
     * 
     * \throw AdapterNotFoundException If not adapter for the specified file name
     *                                 could be found.
     * \throw std::runtime_error       If the adapter fails to import the file.
     */
    Pipeline importPipeline(std::istream& str, const std::string& fileName);
    /*!
     * Imports a pipeline from a file.
     * 
     * Opens the file and calls importPipeline().
     * 
     * \throw AdapterNotFoundException If not adapter for the specified file name
     *                                 could be found.
     * \throw std::runtime_error       If the adapter fails to import the file.
     * \throw std::runtime_error       If the file cannot be opened.
     */
    Pipeline importPipelineFromFile(const std::string& fileName);
    void exportPipeline(const Pipeline& pipeline, std::ostream& str, const std::string& fileName);
    void exportPipelineToFile(const Pipeline& pipeline, const std::string& fileName);
public: // tool registry
    /*!
     * Registers a tool to be used within pipelines later.
     * 
     * \throw DuplicateToolException If there already is a tool with the same name.
     */
    void registerTool(std::unique_ptr<Tool>&& tool);
    /*!
     * Convenience wrapper of registerTool(std::unique_ptr<Tool>).
     *
     * Note that the registry takes ownership of the passed tool. Make sure it is not
     * deleted by any other means after being registrated here.
     *
     * \see registerTool(std::unique_ptr<Tool>)
     * 
     * \throw DuplicateToolException If there already is a tool with the same name.
     */
    inline void registerTool(Tool* tool) {
        registerTool(std::unique_ptr<Tool>(tool));
    }
    /*!
     * Attempts to find a tool by name.
     * 
     * Returns nullptr if no tool with that name could be found.
     *
     * \see registerTool()
     * \see Tool::name()
     */
    [[nodiscard]]
    Tool* findToolByName(const std::string& name) const;
public: // adapter registry
    /*!
     * Registers an adapter to be used for importing resources later.
     */
    void registerAdapter(std::unique_ptr<Adapter>&& adapter);
    /*!
     * Convenience wrapper of registerAdapter(std::unique_ptr<Adapter>).
     *
     * Note that the registry takes ownership of the passed adapter. Make sure it is not
     * deleted by any other means after being registrated here.
     *
     * \see registerAdapter(std::unique_ptr<Adapter>)
     */
    inline void registerAdapter(Adapter* adapter) {
        registerAdapter(std::unique_ptr<Adapter>(adapter));
    }
    /*!
     * Retrieves all adapters for the specified class.
     * 
     * May include a file name parameter to automatically filter out adapters
     * that do no support that file.
     */
    [[nodiscard]]
    std::vector<Adapter*> findAdapters(const ObjectClass& objectClass,
            const std::string& fileName = "") const;
    /*!
     * Attempts to deserialize a resource from a blob of data.
     * 
     * Sequentially tries the adapters registered for the specified objectClass
     * until it returns a non-null value.
     * 
     * Returns nullptr if no matching Adapter could be found.
     */
    [[nodiscard]]
    ObjectPtr<> objectFromBlobImpl(const Blob& blob, const ObjectClass& objectClass,
            const std::string& fileName) const;
    
    template<typename T = Object>
    ObjectPtr<T> objectFromBlob(const Blob& blob, const ObjectClass& objectClass,
            const std::string& fileName) const
    {
        return std::dynamic_pointer_cast<T>(objectFromBlobImpl(blob, objectClass, fileName));
    }
    /*!
     * Attempts to serialize a resource to a blob of data.
     * 
     * Uses the provided fileName to find a suitable adapter for export.
     * 
     * Returns an empty blob if the operation fails.
     */
    Blob objectToBlob(const Object& object, const std::string& fileName);
public: // converter registry
    void registerConverter(std::unique_ptr<Converter>&& converter);
    inline void registerConverter(Converter* converter) {
        registerConverter(std::unique_ptr<Converter>(converter));
    }
    [[nodiscard]]
    ObjectPtr<> convertObjectImpl(const ObjectPtr<>& source, const ObjectClass& toClass);
    // [[nodiscard]]
    // ObjectPtr<> convertObjectImpl(ObjectPtr<>&& source, const ObjectClass& toClass);
    template<typename T = Object, typename TObjectPtr>
    [[nodiscard]]
    ObjectPtr<T> convertObject(TObjectPtr&& source, const ObjectClass& toClass)
    {
        return std::dynamic_pointer_cast<T>(convertObjectImpl(
            std::forward<TObjectPtr>(source), toClass));
    }
public: // object class registry
    /*!
     * Registers a class to the class registry.
     * 
     * This is required for the class to work correctly in scripting environments.
     * 
     * Note that object class ids must be unqiue. Otherwise an exception will be
     * generated. If the same class is registered twice, a warning is emitted, but
     * no exception is thrown.
     * 
     * \throw DuplicateObjectClassException If there already is a class with the same id.
     * 
     * \see registerDefaultClasses()
     */
    void registerObjectClass(const ObjectClass& objectClass);
    /*!
     * Registers the default (in-built) lapis classes.
     * 
     * This should be called on any context that might use scripts.
     */
    void registerDefaultClasses();
public: // Object implementation
    const ObjectClass& getClass() const noexcept override;
};

/*!
 * Exception that is thrown when a tool with a specified name could not be found.
 *
 * \see Tool::findByName()
 */
class ToolNotFoundException : public std::runtime_error
{
private:
    const std::string toolName_;
public:
    explicit inline ToolNotFoundException(const std::string& toolName)
        : std::runtime_error("Could not find tool: " + toolName), toolName_(toolName) {}
public:
    /*!
     * The name of the tool that could not be found.
     */
    inline const std::string& toolName() const { return toolName_; }
};

/*!
 * Exception that is thrown if a tool name duplicates.
 *
 * \see Context::registerTool()
 */
class DuplicateToolException : public std::runtime_error
{
private:
    const std::string toolName_;
public:
    explicit inline DuplicateToolException(const std::string& toolName)
        : std::runtime_error("Duplicate tool with name: " + toolName), toolName_(toolName) {}
public:
    /*!
     * The name that was attempted to be registered twice.
     */
    inline const std::string& toolName() const { return toolName_; }
};

/*!
 * Exception thrown by various functions that require adapter but could not find one.
 */
class AdapterNotFoundException : public std::runtime_error
{
private:
    const std::string fileName_;
public:
    explicit inline AdapterNotFoundException(const std::string& fileName)
         : std::runtime_error("Missing adapter for file: " + fileName), fileName_(fileName) {}
public:
    /*!
     * The name of the file the user tried to import.
     */
    inline const std::string& fileName() const { return fileName_; }
};

/*!
 * Exception that is thrown if a tool name duplicates.
 *
 * \see Context::registerObjectClass()
 */
class DuplicateObjectClassException : public std::runtime_error
{
private:
    const std::string classId_;
public:
    explicit inline DuplicateObjectClassException(const std::string& classId)
        : std::runtime_error("Duplicate object class with id: " + classId), classId_(classId) {}
public:
    /*!
     * The name that was attempted to be registered twice.
     */
    inline const std::string& classId() const { return classId_; }
};
}

#endif // LAPIS_CONTEXT_HPP_INCLUDED
