
#pragma once

#ifndef LAPIS_PIPELINE_HPP_INCLUDED
#define LAPIS_PIPELINE_HPP_INCLUDED 1

#include <vector>

#include "./compatibility.hpp"
#include "./object.hpp"
#include "./tool.hpp"
#include "./value.hpp"

namespace lapis
{

// forward declare for Pipeline::execute()
class Context;

/*!
 * Describes the relation of two pipeline steps.
 *
 * This basically means that one step uses the output of another step as input.
 */
struct PipelineStepRelation
{
    /*!
     * The name of the output to get the value from.
     */
    std::string nameFrom;
    /*!
     * The name of the input parameter to fill with the value.
     */
    std::string nameTo;
    /*!
     * The pipeline step to get the value from.
     */
    std::size_t stepFrom;
};

/*
 * A constant input value for a pipeline step.
 */
struct PipelineStepConstant
{
    /*!
     * The name of the input parameter to fill with the value.
     */
    std::string name;
    /*!
     * The value to pass to the tool.
     */
    Value value;
};

/*!
 * The input of a single pipeline step.
 *
 * This describes where to get the inputs for the used tool from.
 */
struct PipelineStepInput
{
    /*!
     * Relations to other pipeline steps.
     *
     * Causes the outputs of previous steps to be used as input for another one.
     */
    std::vector<PipelineStepRelation> relations;
    /*!
     * Constant input values.
     *
     * Causes the inputs of a step to be filled with constant values.
     */
    std::vector<PipelineStepConstant> constants;
};

/*!
 * A single step of pipeline execution.
 *
 * Describes a single component of the pipeline. This is a tool plus the requred
 * parameters.
 */
struct PipelineStep
{
    /*!
     * The tool used by this pipeline step.
     */
    Tool* tool;
    /*!
     * The input to the tool. Consists of output of previous steps and/or constants.
     */
    PipelineStepInput input;
    /*!
     * The output of the tool. Filled by Tool::execute() and used by successive steps.
     */
    ToolOutput output;
    /*!
     * Number of users of each output variable.
     * 
     * This number is used to determine if a value has to be copied or can be moved into
     * another steps input.
     */
    std::unordered_map<std::string, std::size_t> outputUsers = {};
};

/*!
 * A pipeline to be executed later.
 * 
 * A pipeline describes the steps to be taken by the application. It is composed
 * of multiple steps that are executed successively.
 */
class Pipeline : public Object
{
public:
    static const constexpr auto CLASS_ID = "lapis.Pipeline";
    LAPIS_DLL static const ObjectClass CLASS;
private:
    std::vector<PipelineStep> steps_;
public: // getters
    inline const std::vector<PipelineStep>& steps() const { return steps_; }
public: // data manipulation
    /*!
     * Appends a step to the pipeline.
     *
     * \param tool  The tool to use for this step.
     * \param input Inputs for this step.
     */
    void appendStep(Tool& tool, const PipelineStepInput& input = {});
    /*!
     * Inserts a step in the middle of the pipeline.
     *
     * The step will be inserted before the provided index.
     * Automatically corrects references in past steps.
     *
     * \param index The index where the step is inserted.
     * \param tool  The tool to use for this step.
     * \param input The input for this step.
     */
    void insertStepAt(std::size_t index, Tool& tool, const PipelineStepInput& input = {});
    /*!
     * Removes a strep from the pipeline.
     *
     * Automatically corrects references in past steps. Also reference to this
     * step will be removed.
     *
     * \param index The index of the step to remove.
     */
    void removeStepAt(std::size_t index);
public: // execution
    /*!
     * Execute the pipeline.
     *
     * Only valid pipelines can be executed. Otherwise a PipelineExecutionException
     * will be thrown.
     *
     * \throw PipelineExecutionException If the pipeline is not valid.
     * \throw PipelineExecutionException If the pipeline fails to execute.
     */
    void execute(Context& context);
private: // private utility
    void prepareExecution();
    void validateStep(std::size_t index, const PipelineStep& step) const;
    void updateIndices(PipelineStep& step, std::size_t fromIndex, int offset) noexcept;
    void removeStepReferences(PipelineStep& step, std::size_t index) noexcept;
    ToolInput generateInput(PipelineStep& step);
public: // Object implementation
    Object* clone() const override;
    const ObjectClass& getClass() const noexcept override;
};

/*!
 * Exception thrown when an invalid step is attempted to be created.
 *
 * A step is invalid if it references another step with a greater index.
 */
class InvalidStepException : public std::logic_error
{
public:
    inline explicit InvalidStepException(const std::string& description)
        : std::logic_error("Attempt to insert invalid pipeline step: " + description) {}
};

/*!
 * Exception thrown when pipeline execution fails for any reason.
 * 
 * 
 */
class PipelineExecutionException : public std::runtime_error
{
public:
    inline explicit PipelineExecutionException(const std::string& description)
        : std::runtime_error("Exception during pipeline execution: " + description) {}
};
}

#endif // LAPIS_PIPELINE_HPP_INCLUDED
