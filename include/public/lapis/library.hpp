
#pragma once

#ifndef LAPIS_LIBRARY_HPP_INCLUDED
#define LAPIS_LIBRARY_HPP_INCLUDED 1

#include <stdexcept>
#include <string>
#include <vector>

#include "./object.hpp"
#include "./version.hpp"

namespace lapis
{
class Context; // forward declare for Library::load()
/*!
 * A library of tools, adapters etc..
 * 
 * Used to expand the functionality of the application by adding different tools.
 */
class Library : public Object
{
public:
    static const constexpr auto CLASS_ID = "lapis.Library";
    static const ObjectClass CLASS;
public:
    static const char* SHARED_LIBRARY_FILE_EXTENSION;
private:
    static const constexpr auto ENTRY_POINT_NAME = "initLibrary";
    using entry_point_t = Library* (*)();
private:
    const std::string id_;
    const Version version_;
protected:
    Library(const std::string& id, const Version& version);
public:
    virtual ~Library();
public: // getters
    /*!
     * Retrieves the libraries id.
     * 
     * The id is a unique identifier (normally <author>.<name>) for this library. It
     * should stay the same between multiple iterations of the same library.
     */
    inline const std::string& id() const { return id_; }
    /*!
     * Retrieves the libraries version.
     * 
     * The version number should be increased for each new iteration.
     */
    inline Version version() const { return version_; }
public: // interface
    /*!
     * Load the library.
     * 
     * Used to load the library into an application context. Tools are normally
     * registered here.
     * 
     * This function must be overriden by any library.
     */
    virtual void load(Context& context) = 0;
    /*!
     * Unload the library.
     * 
     * This function may optionally be overwritten in order to provide deinitilization
     * of the library.
     */
    virtual void unload();
public:
    /*!
     * Loads a library from a shared library (.dll or .so).
     *
     * The extension and format depends on the operating system.
     *
     * \param path The path of the library file (including file extensions etc.).
     *
     * \throw LibraryException If the library could not be loaded.
     *
     * \see SHARED_LIBRARY_FILE_EXTENSION
     */
    static Library* loadFromSharedLibrary(const std::string& path);
    /*!
     * Load any library found in the specified folder.
     * 
     * Libraries are filtered by extension. The folder is not searched
     * recursively. This function instantly returns if the folder does
     * not exist.
     * 
     * If a library cannot be loaded, it is not added to the result vector
     * and an error messages is printed.
     * 
     * \param folder The path to the folder that should be searched.
     */
    static std::vector<Library*> loadAll(const std::string& folder);
public: // Object implementation
    const ObjectClass& getClass() const noexcept override;
};

/*!
 * Exception that is thrown when a library could not be loaded.
 */
class LibraryException : public std::runtime_error
{
public:
    inline LibraryException(const std::string& message)
        : std::runtime_error("Error loading library: " + message) {}
};
}

#endif // LAPIS_LIBRARY_HPP_INCLUDED
