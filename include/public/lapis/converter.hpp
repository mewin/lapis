
#pragma once

#ifndef LAPIS_CONVERTER_HPP_INCLUDED
#define LAPIS_CONVERTER_HPP_INCLUDED 1

#include "./object.hpp"
#include "./util/flags.hpp"

namespace lapis
{
enum class ConverterFlagsBits
{
    BIDIRECTIONAL = 0,
    CAN_MOVE_CONVERT = 1
};

struct ConverterFlags : FlagsBase<ConverterFlagsBits>
{
    // constructors
    ConverterFlags() = default;
    ConverterFlags(const ConverterFlags&) = default;

    // operators
    ConverterFlags& operator=(const ConverterFlags&) = default;

    // flags
    [[nodiscard]]
    constexpr bool bidirectional() const noexcept
    {
        return isFlag(ConverterFlagsBits::BIDIRECTIONAL);
    }

    constexpr ConverterFlags& bidirectional(bool value) noexcept
    {
        setFlag(ConverterFlagsBits::BIDIRECTIONAL, value);
        return *this;
    }

    [[nodiscard]]
    constexpr bool canMoveConvert() const noexcept
    {
        return isFlag(ConverterFlagsBits::CAN_MOVE_CONVERT);
    }

    constexpr ConverterFlags& canMoveConvert(bool value) noexcept
    {
        setFlag(ConverterFlagsBits::CAN_MOVE_CONVERT, value);
        return *this;
    }
};

class Converter : public Object
{
public:
    static const constexpr auto CLASS_ID = "lapis.Converter";
    static const ObjectClass CLASS;
private:
    const ObjectClass& classFrom_;
    const ObjectClass& classTo_;
    const ConverterFlags flags_;
protected:
    Converter(const ObjectClass& classFrom, const ObjectClass& classTo,
        const ConverterFlags& flags = {});
    Converter(const Converter&) = delete;
    Converter(Converter&&) = delete;
public:
    virtual ~Converter();
public: // operators
    Converter& operator=(const Converter&) = delete;
    Converter& operator=(Converter&&) = delete;
public: // getters
    const constexpr ObjectClass& classFrom() const noexcept { return classFrom_; }
    const constexpr ObjectClass& classTo() const noexcept { return classTo_; }
    constexpr ConverterFlags flags() const noexcept { return flags_; }
public: // utility
    [[nodiscard]]
    inline const ObjectClass& otherClass(const ObjectClass& oneClass) const noexcept
        { return oneClass == classFrom_ ? classTo_ : classFrom_; }
public: // interface
    virtual ObjectPtr<> convert(const Object& source) const = 0;
    virtual ObjectPtr<> convert(Object&& source) const;
public: // Object implementation
    const ObjectClass& getClass() const noexcept override;
};
}

#endif // LAPIS_CONVERTER_HPP_INCLUDED
