
#pragma once

#ifndef LAPIS_FUNCTION_HPP_INCLUDED
#define LAPIS_FUNCTION_HPP_INCLUDED 1

#include <functional>
#include <vector>
#include <tuple>
#include <type_traits>

#include "./value.hpp"
#include "./util/function_traits.hpp"

namespace lapis
{
namespace detail
{
// black template magic to automatically wrap C++ functions in callable wrappers
/*!
 * Function wrapper used by the CppFunction::wrap() and
 * CppFunction::wrapExplicit() methods.
 */
template<typename TFn, typename... TArgs>
struct WrappedFunction
{
    TFn func_;
    using result_t = std::invoke_result_t<TFn, TArgs...>;

    explicit inline WrappedFunction(TFn func) : func_(func) {}

    /*!
     * Retrieves the function argument at a given position and casts it to the
     * corresponding type using Value::as().
     */
    template<std::size_t pos>
    auto argumentAt(const std::vector<Value>& arguments)
    {
        // Retrieve argument type using std::tuple_element_t ...
        using arg_type = std::decay_t<std::tuple_element_t<pos, std::tuple<TArgs...>>>;
        // ... and cast the value at pos.
        if (pos < arguments.size())
        {
            return arguments[pos].as<arg_type>();
        }
        // TODO: throw error? default values for parameters?
        return Value().as<arg_type>();
    }

    /*!
     * Helper template to actually call the function.
     * 
     * This function is passed a sequence of std::size_t corresponding to the
     * number of arguments the function takes. Those are passed to argumentAt()
     * to retrieve the function parameters.
     */
    template<std::size_t... seq>
    auto callImpl(const std::vector<Value>& arguments, std::index_sequence<seq...>)
    {
        if constexpr (std::is_same_v<result_t, void>)
        {
            func_(argumentAt<seq>(arguments) ...);
        }
        else
        {
            return func_(argumentAt<seq>(arguments) ...);
        }
    }

    /*!
     * Calls the wrapped function.
     * 
     * Generates an index_sequence and passes it to callImpl() where the function
     * is actually called.
     */
    auto call(const std::vector<Value>& arguments)
    {
        if constexpr (std::is_same_v<result_t, void>)
        {
            callImpl(arguments, std::make_index_sequence<sizeof...(TArgs)>());
        }
        else
        {
            return callImpl(arguments, std::make_index_sequence<sizeof...(TArgs)>());
        }
    }

    /*!
     * Call operator. Calls the wrapped function, wrapps the result in a Value
     * and wrapps that Value in a vector.
     */
    Value operator()(const std::vector<Value>& arguments)
    {
        if constexpr (std::is_same_v<result_t, void>)
        {
            call(arguments);
            return Value();
        }
        else
        {
            auto result = call(arguments);
            return Value(result);
        }
    }
};
}

class Function
{
public:
    virtual ~Function();
public: // interface
    virtual Value invoke(const std::vector<Value>& arguments) = 0;
    
    template<typename... TArgs>
    Value call(TArgs&&... args)
    {
        std::vector<Value> arguments;
        arguments.reserve(sizeof...(TArgs));
        (arguments.push_back(args), ...);
        return invoke(arguments);
    }
};

class CppFunction : public Function
{
private:
    using wrapped_t = std::function<Value(const std::vector<Value>&)>;
private:
    wrapped_t wrapped_;
public:
    explicit inline CppFunction(wrapped_t wrapped) noexcept : wrapped_(wrapped) {}
    CppFunction(const CppFunction&) = default;
    CppFunction(CppFunction&&) = default;
public: // implementation
    Value invoke(const std::vector<Value>& arguments) override;
private: // private utility
    template<typename TFunc, typename... TArgs>
    static FunctionPtr wrapImpl(TFunc&& func, util::argument_traits<TArgs...> = {})
    {
        using func_t = std::remove_reference_t<decltype(func)>;
        static_assert(std::is_invocable_v<func_t, TArgs...>, "Supplied function not invokable using given argument types.");
        return std::make_shared<CppFunction>(detail::WrappedFunction<func_t, TArgs...>(func));
    }
public: // utility
    // template<typename... TArgs>
    // static FunctionPtr wrapExplicit(auto&& func)
    // {
    //     return wrapImpl<TArgs...>(func);
    // }

    template<typename TFunc>
    static FunctionPtr wrap(TFunc&& func)
    {
        using traits = util::function_traits<std::decay_t<TFunc>>;
        return wrapImpl(func, typename traits::argument_types());
    }
};
}

#endif // LAPIS_FUNCTION_HPP_INCLUDED
