
#pragma once

#ifndef LAPIS_TYPES_HPP_INCLUDED
#define LAPIS_TYPES_HPP_INCLUDED 1

#include <memory>
#include <span>
#include <map>
#include <string>
#include <vector>

namespace lapis
{
class Function;
class Object;
class Value;

using Blob = std::vector<char>;
using BlobView = std::span<char>;
using ConstBlobView = std::span<const char>;
using FunctionPtr = std::shared_ptr<Function>;
template<typename TObject = Object>
using ObjectPtr = std::shared_ptr<TObject>;
using Map = std::map<std::string, Value>;
using Vector = std::vector<Value>;
}

#endif // LAPIS_TYPES_HPP_INCLUDED
