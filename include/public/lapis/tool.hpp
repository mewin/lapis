
#pragma once

#ifndef LAPIS_TOOL_HPP_INCLUDED
#define LAPIS_TOOL_HPP_INCLUDED 1

#include <stdexcept>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "./object.hpp"
#include "./value.hpp"

namespace lapis
{
class Context; // cyclic dependency
/*!
 * The input provided to a tool.
 *
 * \see Tool::execute()
 */
struct ToolInput
{
    std::unordered_map<std::string, Value> parameters;
};

/*!
 * The output of a tool.
 *
 * \see Tool::execute()
 */
struct ToolOutput
{
    std::unordered_map<std::string, Value> results;
};

/*!
 * Description of a single tool parameter.
 */
struct ToolParameter
{
    /*!
     * The name of this parameter.
     */
    std::string name;
    /*!
     * The type of value this parameter expects.
     */
    ValueType type;
    /*!
     * The object class this parameter takes. Ignored if type is not OBJECT.
     */
    const ObjectClass* objectClass = nullptr;
};

/*!
 * Description of a single tool output.
 */
struct ToolResult
{
    /*!
     * The name of the output.
     */
    std::string name;
    /*!
     * The type of output that is produced.
     */
    ValueType type;
    /*!
     * The object class of the produced output. Ignored if type is not OBJECT.
     */
    const ObjectClass* objectClass = nullptr;
};

/*!
 * A basic tool that can be used within a pipeline.
 *
 * This describes a single step to be taken in a pipeline like reading a file,
 * resizing an image, etc.
 *
 * Names of tools should normally be organized as `<author>.<library>.<tool>`.
 * Author and library will be used to sort the tool in the UI while the last
 * part is displayed as the tools name.
 */
class Tool : public Object
{
public:
    static const constexpr auto CLASS_ID = "lapis.Tool";
    static const ObjectClass CLASS;
private:
    const std::string name_;
    const std::vector<ToolParameter> parameters_;
    const std::vector<ToolResult> results_;
protected:
    explicit inline Tool(const std::string& name, const std::vector<ToolParameter>& parameters,
        const std::vector<ToolResult>& results)
        : name_(name), parameters_(parameters), results_(results) {}
public:
    virtual ~Tool();
public: // getters and setters
    /*!
     * The name of the tool.
     *
     * This name is used to identify the tool in serialized pipelines.
     * Therefore the name of a tool must be unique within the application.
     */
    [[nodiscard]]
    constexpr const std::string& name() const noexcept { return name_; }
    /*!
     * The parameters the tool takes.
     */
    [[nodiscard]]
    constexpr const std::vector<ToolParameter>& parameters() const noexcept { return parameters_; }
    /*!
     * The results this tool produces.
     */
    [[nodiscard]]
    constexpr const std::vector<ToolResult>& results() const noexcept { return results_; }
public: // functionality
    /*!
     * Execute this tool.
     *
     * Virtual method that is to be overwritten by specific tools.
     *
     * \param context The context/global state of the current pipeline.
     * \param input   Tool input.
     * \param output  Tool output.
     * \throw ToolExecutionException If there is a problem while executing the tool.
     */
    virtual void execute(Context& context, const ToolInput& input, ToolOutput& output) = 0;
public: // Object implementation
    const ObjectClass& getClass() const noexcept override;
};

/*!
 * Exception that is thrown if there are problems while executing a tool.
 *
 * \see Tool::execute()
 */
class ToolExecutionException : public std::runtime_error
{
private:
    Tool& tool_;
public:
    inline ToolExecutionException(Tool& tool, const std::string& message)
        : std::runtime_error("Error during tool execution: " + message), tool_(tool) {}
public:
    /*!
     * The tool that caused the exception.
     */
    inline Tool& tool() const { return tool_; }
};
}

#endif // LAPIS_TOOL_HPP_INCLUDED
