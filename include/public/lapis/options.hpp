
#pragma once

#ifndef LAPIS_OPTIONS_HPP_INCLUDED
#define LAPIS_OPTIONS_HPP_INCLUDED 1

#include "./value.hpp"

#include <unordered_map>

namespace lapis
{
class Options
{
private:
    Map values_;
public:
    inline const Value& get(const std::string& name) const
    {
        auto it = values_.find(name);
        if (it == values_.end())
        {
            return Value::EMPTY;
        }
        return it->second;
    }

    template<typename T>
    inline T getAs(const std::string& name, T defaultValue = T()) const
    {
        auto it = values_.find(name);
        if (it == values_.end())
        {
            return defaultValue;
        }
        return it->second.as<T>();
    }
};
}

#endif // LAPIS_OPTIONS_HPP_INCLUDED
