
#pragma once

#ifndef LAPIS_SCRIPT_HPP_INCLUDED
#define LAPIS_SCRIPT_HPP_INCLUDED 1

#include "./context.hpp"
#include "./function.hpp"
#include "./object.hpp"

namespace lapis
{
class Script : public Object
{
public:
    static const constexpr auto CLASS_ID = "lapis.Script";
    LAPIS_DLL static const ObjectClass CLASS;
public:
    virtual ~Script();
public: // functionality
    void initApi(const Context& context);
public: // interface
    virtual std::vector<Value> execute(const std::vector<Value>& arguments = {}) = 0;
    virtual void registerObjectClass(const ObjectClass& objectClass) = 0;
    virtual void setGlobal(const std::string& name, const Value& value) = 0;
    virtual Value getGlobal(const std::string& name) = 0;
public: // Object implementation
    const ObjectClass& getClass() const noexcept override;
};
}

#endif // LAPIS_SCRIPT_HPP_INCLUDED
