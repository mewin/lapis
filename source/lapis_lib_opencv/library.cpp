
#include <string>

#include "lapis/context.hpp"
#include "lapis/library.hpp"
#include "lapis/version.hpp"
#include "lapis/util/macros.hpp"

namespace lapis::libopencv
{
class LibOpenCV : public Library
{
public:
    static const constexpr auto           ID      = "lapis.opencv";
    static const constexpr lapis::Version VERSION = { 0, 0, 1 };
public:
    inline LibOpenCV() : Library(ID, VERSION) {}
public: // interface
    void load(Context& context) override;
};

void LibOpenCV::load(Context& context)
{
    (void) context;
}
}

LAPIS_LIBRARY_ENTRY_POINT(opencv)
{
    
    return new lapis::libopencv::LibOpenCV();
}
