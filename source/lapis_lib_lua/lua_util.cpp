
#include "lapis_lib_lua/lua_util.hpp"

#include "lapis_lib_lua/lua_function.hpp"

namespace lapis::liblua
{
static const constexpr char* TNAME_FUNCTION = "__function__";
static const constexpr char* TNAME_OBJECT   = "__object__";

static int l_callFunction(lua_State* state)
{
    const void* funcptr = luaL_checkudata(state, 1, TNAME_FUNCTION);
    const FunctionPtr& function = *static_cast<const FunctionPtr*>(funcptr);

    const Vector arguments = getValues(state, 2, -1);
    const Value result = function->invoke(arguments);

    if (result.empty())
    {
        return 0;
    }
    else
    {
        pushValue(state, result);
        return 1;
    }
}

static int l_gcObject(lua_State* state)
{
    const void* objptr = luaL_checkudata(state, 1, TNAME_OBJECT);
    const ObjectPtr<>& object = *static_cast<const ObjectPtr<>*>(objptr);

    object.~shared_ptr();

    return 0;
}

static int l_gcFunction(lua_State* state)
{
    const void* funcptr = luaL_checkudata(state, 1, TNAME_FUNCTION);
    const FunctionPtr& function = *static_cast<const FunctionPtr*>(funcptr);

    function.~shared_ptr();

    return 0;
}

int normalizeIndex(lua_State* state, int index) noexcept
{
    if (index >= 0)
    {
        return index;
    }
    else
    {
        return lua_gettop(state) + index + 1;
    }
}

void* newAlignedUserdata(lua_State* state, std::size_t size, std::size_t alignment)
{
    std::size_t space = size + alignment - 1;
    void* pointer = lua_newuserdata(state, space);
    return std::align(alignment, size, pointer, space);
}

void pushAllValues(lua_State* state, const Vector& values)
{
    for (const Value& value : values)
    {
        pushValue(state, value);
    }
}

void pushValueVector(lua_State* state, const Vector& values)
{
    int index = 1;
    lua_newtable(state);

    for (const Value& value : values)
    {
        lua_pushinteger(state, index);
        pushValue(state, value);
        lua_settable(state, -3);

        ++index;    
    }
}

void pushValueMap(lua_State* state, const Map& values)
{
    lua_newtable(state);

    for (const auto& entry : values)
    {
        lua_pushstring(state, entry.first.c_str());
        pushValue(state, entry.second);
        lua_settable(state, -3);
    }
}

void pushValueObject(lua_State* state, const ObjectPtr<>& value)
{
    if (!value)
    {
        lua_pushnil(state);
    }

    createUserObject<ObjectPtr<>>(state, value);
    luaL_newmetatable(state, TNAME_OBJECT);

    lua_pushstring(state, "__gc");
    lua_pushcfunction(state, &l_gcObject);
    lua_settable(state, -3);

    lua_setmetatable(state, -2);
}

void pushFunction(lua_State* state, const FunctionPtr& value)
{
    if (!value)
    {
        lua_pushnil(state);
        return;
    }

    createUserObject<FunctionPtr>(state, value);
    luaL_newmetatable(state, TNAME_FUNCTION);

    lua_pushstring(state, "__call");
    lua_pushcfunction(state, &l_callFunction);
    lua_settable(state, -3);

    lua_pushstring(state, "__gc");
    lua_pushcfunction(state, &l_gcFunction);
    lua_settable(state, -3);

    lua_setmetatable(state, -2);
}

void pushValue(lua_State* state, const Value& value)
{
    switch (value.type())
    {
        case ValueType::NONE:
            lua_pushnil(state);
            break;
        case ValueType::BOOLEAN:
            lua_pushboolean(state, value.asBool());
            break;
        case ValueType::INTEGER:
            lua_pushinteger(state, value.asInteger());
            break;
        case ValueType::DOUBLE:
            lua_pushnumber(state, value.asDouble());
            break;
        case ValueType::STRING:
            lua_pushstring(state, value.asString().c_str());
            break;
        case ValueType::VECTOR:
            pushValueVector(state, value.asVector());
            break;
        case ValueType::MAP:
            pushValueMap(state, value.asMap());
            break;
        case ValueType::OBJECT:
            pushValueObject(state, value.asObject());
            break;
        case ValueType::BLOB:
        case ValueType::FUNCTION:
            pushFunction(state, value.asFunction());
            break;
        case ValueType::ANY:
            lua_pushnil(state);
            break;
    }
}

Vector getValues(lua_State* state, int idxFirst, int idxLast)
{
    idxFirst = normalizeIndex(state, idxFirst);
    idxLast = normalizeIndex(state, idxLast);

    if (idxFirst > idxLast)
    {
        return {};
    }

    Vector result;
    result.reserve(idxLast - idxFirst + 1);

    for (int idx = idxFirst; idx <= idxLast; ++idx)
    {
        result.push_back(getValue(state, idx));
    }

    return result;
}

Value getValue(lua_State* state, int idx)
{
    switch (lua_type(state, idx))
    {
        case LUA_TNIL:
            return Value();
        case LUA_TBOOLEAN:
            return lua_toboolean(state, idx) != 0;
        case LUA_TNUMBER:
            return lua_tonumber(state, idx);
        case LUA_TSTRING:
            return std::string(lua_tostring(state, idx));
        case LUA_TTABLE:
            return convertTable(state, idx);
        case LUA_TFUNCTION:
            return LuaFunction::wrap(state, idx);
        case LUA_TUSERDATA:
            if (void* ptrFunc = luaL_testudata(state, idx, TNAME_FUNCTION))
            {
                return *static_cast<FunctionPtr*>(ptrFunc);
            }
            else if (void* ptrObject = luaL_testudata(state, idx, TNAME_OBJECT))
            {
                return *static_cast<ObjectPtr<>*>(ptrObject);
            }
            return Value();
        default:
            return Value();
    }
}

Value convertTable(lua_State* state, int idx)
{
    if (!lua_istable(state, idx))
    {
        return Value();
    }
    idx = normalizeIndex(state, idx);

    Map result;

    lua_pushnil(state);
    while (lua_next(state, idx) != 0)
    {
        Value key = getValue(state, -2);
        Value value = getValue(state, -1);

        result[key.asString()] = value;

        lua_pop(state, 1);
    }

    return result;
}
}