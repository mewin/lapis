
#include "lapis_lib_lua/adapter_script_lua.hpp"

#include "lapis_lib_lua/lua_script.hpp"

namespace lapis::liblua
{
AdapterScriptLua::AdapterScriptLua() : Adapter(
    Script::CLASS,
    AdapterFlags().canImport(true)
)
{

}

bool AdapterScriptLua::canHandle(const Context& /* context */, const std::string& fileName) const noexcept // override
{
    return fileName.ends_with(".lua");
}

ObjectPtr<> AdapterScriptLua::fromBlob(const Context& context, const Blob& blob,
        const std::string& /* fileName */) // override
{
    std::shared_ptr<LuaScript> script = std::make_shared<LuaScript>();
    script->initApi(context);
    script->loadBlob(blob);

    return script;
}
}
