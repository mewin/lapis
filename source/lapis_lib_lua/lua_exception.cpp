
#include "lapis_lib_lua/lua_exception.hpp"

#include <fmt/format.h>
#include <lua.hpp>

namespace lapis::liblua
{
static std::string formatLuaError(lua_State* state, int error)
{
    std::string type = "Unkonwn";
    switch (error)
    {
        case LUA_ERRRUN:
            type = "Runtime Error";
            break;
        case LUA_ERRSYNTAX:
            type = "Syntax Error";
            break;
        case LUA_ERRMEM:
            type = "Memory Error";
            break;
        case LUA_ERRERR:
            type = "Error Handling Error";
            break;
    }
    std::string message;
    if (lua_isstring(state, -1))
    {
        message = luaL_tolstring(state, -1, nullptr);
    }
    
    return fmt::format("Type: {}; Message: {}", type, message);
}

LuaException::LuaException(lua_State* state, int error) :
    LuaException(formatLuaError(state, error))
{
    
}
}
