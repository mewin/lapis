
#include "lapis_lib_lua/lua_script.hpp"

#include <lua.hpp>

#include "lapis_lib_lua/lua_exception.hpp"
#include "lapis_lib_lua/lua_util.hpp"

namespace lapis::liblua
{
LuaScript::LuaScript()
{
    handle_ = luaL_newstate();
    luaL_openlibs(handle_);

    if (handle_ == nullptr)
    {
        throw LuaException("Error allocating lua state.");
    }
}

LuaScript::~LuaScript()
{
    if (handle_ != nullptr)
    {
        lua_close(handle_);
    }
}

void LuaScript::loadString(const std::string& code)
{
    checkHandle();
    checkError(luaL_loadstring(handle_, code.c_str()));
}

void LuaScript::loadBlob(const Blob& code)
{
    checkHandle();

    const char* buffer = code.data();
    std::size_t bufferSize = code.size();
    checkError(luaL_loadbuffer(handle_, buffer, bufferSize, "blob"));
}

std::vector<Value> LuaScript::execute(const std::vector<Value>& arguments) // override
{
    // executes any chunk that has been loaded yet
    // the stack should only contain callables
    // returns only the result of the last chunk
    checkHandle();

    std::vector<Value> results;

    const int nFunctions = lua_gettop(handle_);
    for (int i = 0; i < nFunctions; ++i)
    {
        lua_pushvalue(handle_, i + 1); // push the chunk to call to the top
        pushAllValues(handle_, arguments); // push the arguments
        checkError(lua_pcall(handle_, static_cast<int>(arguments.size()), LUA_MULTRET, 0)); // call

        // anything left on the stack is return values
        if (i == nFunctions - 1)
        {
            results = getValues(handle_, nFunctions + 1, -1);
        }

        // reset stack
        lua_settop(handle_, nFunctions);
    }

    return results;
}

void LuaScript::registerObjectClass(const ObjectClass& /* objectClass */) // override
{
    
}


void LuaScript::setGlobal(const std::string& name, const Value& value) // override
{
    checkHandle();

    pushValue(handle_, value);
    lua_setglobal(handle_, name.c_str());
}

Value LuaScript::getGlobal(const std::string& name) // override
{
    checkHandle();

    lua_getglobal(handle_, name.c_str()); 
    Value result = getValue(handle_, -1);
    lua_pop(handle_, 1);

    return result;
}

void LuaScript::checkHandle() const
{
    if (handle_ == nullptr)
    {
        throw LuaException("Uninitialized lua state.");
    }
}

void LuaScript::checkError(int err) const
{
    checkLuaError(handle_, err);
}
}
