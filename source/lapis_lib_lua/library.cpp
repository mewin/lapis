
#include <string>

#include "lapis/context.hpp"
#include "lapis/library.hpp"
#include "lapis/version.hpp"
#include "lapis/util/macros.hpp"

#include "lapis_lib_lua/adapter_script_lua.hpp"

namespace lapis::liblua
{
class LibLua : public Library
{
public:
    static const constexpr auto           ID      = "lapis.lua";
    static const constexpr lapis::Version VERSION = { 0, 0, 1 };
public:
    inline LibLua() : Library(ID, VERSION) {}
public: // interface
    void load(Context& context) override;
};

void LibLua::load(Context& context)
{
    context.registerAdapter(new AdapterScriptLua());
}
}

LAPIS_LIBRARY_ENTRY_POINT(lua)
{
    
    return new lapis::liblua::LibLua();
}
