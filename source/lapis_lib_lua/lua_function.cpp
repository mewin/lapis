
#include "lapis_lib_lua/lua_function.hpp"

#include <cassert>

#include <lua.hpp>

#include "lapis_lib_lua/lua_util.hpp"

namespace lapis::liblua
{
static const constexpr char* REGISTRY_KEY_FUNCTIONS = "__functions__";
static const constexpr char* KEY_NEXT_INDEX = "__next_index";

static void getFunctionsTable(lua_State* state)
{
    lua_pushstring(state, REGISTRY_KEY_FUNCTIONS);
    lua_gettable(state, LUA_REGISTRYINDEX);

    if (lua_istable(state, -1)) // already initialized
    {
        return;
    }
    lua_pop(state, 1);
    
    /// not initialized yet -> initialize
    // push name and table
    lua_pushstring(state, REGISTRY_KEY_FUNCTIONS);
    lua_newtable(state);

    // set table
    lua_settable(state, LUA_REGISTRYINDEX);

    // put it back onto the stack
    lua_pushstring(state, REGISTRY_KEY_FUNCTIONS);
    lua_gettable(state, LUA_REGISTRYINDEX);

    // init next index field
    lua_pushstring(state, KEY_NEXT_INDEX);
    lua_pushinteger(state, 1);
    lua_settable(state, -3);
    
    assert(lua_istable(state, -1));
}

LuaFunction::LuaFunction(lua_State* state, int stackIdx) : state_(state)
{
    stackIdx = normalizeIndex(state, stackIdx);

    getFunctionsTable(state_);
    lua_pushstring(state_, KEY_NEXT_INDEX);
    lua_gettable(state_, -2);

    // retrieve next index
    assert(lua_isinteger(state_, -1));
    const lua_Integer nextIndex = lua_tointeger(state_, -1);
    lua_pop(state_, 1);

    // increate next index
    lua_pushstring(state_, KEY_NEXT_INDEX);
    lua_pushinteger(state_, nextIndex + 1);
    lua_settable(state_, -3);

    // save the function
    lua_pushinteger(state_, nextIndex);
    lua_pushvalue(state_, stackIdx);
    lua_settable(state_, -3);

    // remove registry table from stack again
    lua_pop(state_, 1);

    functionIndex_ = static_cast<std::size_t>(nextIndex);
}

Value LuaFunction::invoke(const std::vector<Value>& arguments) // override
{
    if (functionIndex_ == 0)
    {
        // TODO: warning
        return Value(); // invalidated
    }

    const int stackSize = lua_gettop(state_); // store stack size to reset it later
    
    // retrieve the function
    getFunctionsTable(state_);
    lua_pushinteger(state_, functionIndex_);
    lua_gettable(state_, -2);

    // just try to call it
    pushAllValues(state_, arguments);
    checkLuaError(state_, lua_pcall(state_, static_cast<int>(arguments.size()), LUA_MULTRET, 0));

    // collect the results
    Vector results = getValues(state_, stackSize + 1, -1);

    // reset stack and return
    lua_settop(state_, stackSize);
    return results;
}
}
