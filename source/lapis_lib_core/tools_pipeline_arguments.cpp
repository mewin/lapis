
#include "lapis_lib_core/tools_pipeline_arguments.hpp"

#include <iterator>

#include "lapis/context.hpp"

namespace lapis::libcore
{
void ToolPipelineArgument::execute(Context& context, const ToolInput& input,
    ToolOutput& output) // override
{
    std::string paramName = input.parameters.at(INPUT_NAME).asString();

    auto it = context.arguments().find(paramName);
    if (it == context.arguments().end())
    {
        throw PipelineExecutionException("Missing parameter: " + paramName);
    }
    output.results[OUTPUT_VALUE] = it->second;
}

void ToolPipelineArguments::execute(Context& context, const ToolInput& /* input */,
    ToolOutput& output) // override
{
    std::map<std::string, Value> values;
    for (const auto& entry : context.arguments()) {
        values.insert(std::make_pair(entry.first, Value(entry.second)));
    }
    output.results[OUTPUT_VALUES] = std::move(values);
}
}
