
#include <string>

#include "lapis/context.hpp"
#include "lapis/library.hpp"
#include "lapis/version.hpp"
#include "lapis/util/macros.hpp"

#include "lapis_lib_core/adapter_pipeline_script.hpp"
#include "lapis_lib_core/tools_bitmap_io.hpp"
#include "lapis_lib_core/tools_file_io.hpp"
#include "lapis_lib_core/tools_pipeline_arguments.hpp"

namespace lapis::libcore
{
class LibCore : public Library
{
public:
    static const constexpr auto           ID      = "lapis.core";
    static const constexpr lapis::Version VERSION = { 0, 0, 1 };
public:
    inline LibCore() : Library(ID, VERSION) {}
public: // interface
    void load(Context& context) override;
};

void LibCore::load(Context& context)
{
    context.registerAdapter(new AdapterPipelineScript());
    
    context.registerTool(new ToolReadTextFile());
    context.registerTool(new ToolWriteTextFile());
    context.registerTool(new ToolReadBinaryFile());
    context.registerTool(new ToolWriteBinaryFile());
    context.registerTool(new ToolBitmapFromBlob());
    context.registerTool(new ToolBitmapToBlob());
    
    context.registerTool(new ToolPipelineArgument());
    context.registerTool(new ToolPipelineArguments());
}
}

LAPIS_LIBRARY_ENTRY_POINT(core)
{
    
    return new lapis::libcore::LibCore();
}
