
#include "lapis_lib_core/adapter_pipeline_script.hpp"

#include <filesystem>

#include "lapis/script.hpp"
#include "lapis/util/io.hpp"

namespace fs = std::filesystem;

namespace lapis::libcore
{
// small class that is used to allow scripts to directly set inputs to other tools outputs
const ObjectClass STEP_OUTPUT_REFERENCE_CLASS{"lapis.core._step_output_reference", nullptr};
class StepOutputReference : public Object
{
public:
    const std::string name_;
    const std::size_t step_;
public:
    inline StepOutputReference(std::size_t step, const std::string& name) noexcept
        : name_(name), step_(step) {}
    Object* clone() const override
    {
        return new StepOutputReference(step_, name_);
    }
    const ObjectClass& getClass() const noexcept override
    {
        return STEP_OUTPUT_REFERENCE_CLASS;
    }
};

static Map scriptStep(const Context& context, Pipeline& pipeline, const std::string& toolName, const Map& input)
{
    PipelineStepInput stepInput;
    Tool* tool = context.findToolByName(toolName);

    if (tool == nullptr)
    {
        throw ToolNotFoundException(toolName);
    }

    for (const auto& entry : input)
    {
        if (StepOutputReference* outref = entry.second.as<StepOutputReference*>())
        { // passed the output of another tool -> create relation
            stepInput.relations.push_back({
                .nameFrom = outref->name_,
                .nameTo = entry.first,
                .stepFrom = outref->step_
            });
        }
        else
        { // otherwise set constant input
            stepInput.constants.push_back({
                .name = entry.first,
                .value = entry.second
            });
        }
    }

    pipeline.appendStep(*tool, stepInput);

    // produce result
    // returns the created steps outputs in a map
    const std::size_t stepIdx = pipeline.steps().size() - 1;
    Map toolResults;

    for (const ToolResult& result : tool->results())
    {
        toolResults[result.name] = std::make_shared<StepOutputReference>(stepIdx, result.name);
    }
    return toolResults;
}

AdapterPipelineScript::AdapterPipelineScript() : Adapter(
    Pipeline::CLASS,
    AdapterFlags().canImport(true)
)
{

}

bool AdapterPipelineScript::canHandle(const Context& context, const std::string& fileName) const noexcept // override
{
    return !context.findAdapters(Script::CLASS, fileName).empty(); // is there any Script adapter for this extension?
}

ObjectPtr<> AdapterPipelineScript::fromBlob(const Context& context, const Blob& blob, const std::string& fileName) // override
{
    // create the script
    std::shared_ptr<Script> script = context.objectFromBlob<Script>(blob, Script::CLASS, fileName);

    if (!script)
    {
        throw std::runtime_error("Could not create script for file: " + fileName);
    }

    std::shared_ptr<Pipeline> pipeline = std::make_shared<Pipeline>();
    // add step() script function to allow the script to create a pipeline
    auto fnStep = [&](const std::string& tool,
            const Map& input)
    {
        return scriptStep(context, *pipeline, tool, input);
    };
    script->setGlobal("step", CppFunction::wrap(fnStep));

    // run the script
    script->execute();

    return pipeline;
}
}
