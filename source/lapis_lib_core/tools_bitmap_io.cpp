
#include "lapis_lib_core/tools_bitmap_io.hpp"

#include "lapis/context.hpp"

namespace lapis::libcore
{
void ToolBitmapFromBlob::execute(Context& context, const ToolInput& input, ToolOutput& output) // override
{
    const Blob& data = input.parameters.at(INPUT_DATA).asBlob();
    output.results[OUTPUT_BITMAP] = context.objectFromBlob<types::Bitmap>(data,
            types::Bitmap::CLASS, ""); // TODO: fileName parameter (requires optional parameters)
}

void ToolBitmapToBlob::execute(Context& context, const ToolInput& input, ToolOutput& output) // override
{
    const ObjectPtr<types::Bitmap> bitmap = input.parameters.at(INPUT_BITMAP)
            .as<ObjectPtr<types::Bitmap>>();
    const std::string& fileName = input.parameters.at(INPUT_FILENAME).asString();
    output.results[OUTPUT_DATA] = context.objectToBlob(*bitmap, fileName);
}
}
