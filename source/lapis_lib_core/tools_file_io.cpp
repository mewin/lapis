
#include "lapis_lib_core/tools_file_io.hpp"

#include <fstream>

namespace lapis::libcore
{
void ToolReadTextFile::execute(Context& /* context */, const ToolInput& input,
    ToolOutput& output)
{
    const std::string& fileName = input.parameters.at(INPUT_PATH).asString();

    std::ifstream str(fileName, std::ios::in | std::ios::ate); // start at end
    std::ifstream::pos_type size = str.tellg();

    str.seekg(0, std::ios::beg); // back to the beginning

    std::string result;
    result.resize(size);
    
    str.read(result.data(), size);

    output.results[OUTPUT_TEXT] = result;
}

void ToolWriteTextFile::execute(Context& /* context */, const ToolInput& input,
    ToolOutput& /* output */)
{
    const std::string& fileName = input.parameters.at(INPUT_PATH).asString();
    const std::string& text = input.parameters.at(INPUT_TEXT).asString();

    std::ofstream str(fileName, std::ios::out | std::ios::trunc);

    str.write(text.c_str(), text.size());
}

void ToolReadBinaryFile::execute(Context& /* context */, const ToolInput& input,
    ToolOutput& output)
{
    const std::string& fileName = input.parameters.at(INPUT_PATH).asString();

    std::ifstream str(fileName, std::ios::in | std::ios::ate | std::ios::binary); // start at end
    std::ifstream::pos_type size = str.tellg();

    str.seekg(0, std::ios::beg); // back to the beginning

    Blob result;
    result.resize(size);
    
    str.read(result.data(), size);

    output.results[OUTPUT_DATA] = result;
}

void ToolWriteBinaryFile::execute(Context& /* context */, const ToolInput& input,
    ToolOutput& /* output */)
{
    const std::string& fileName = input.parameters.at(INPUT_PATH).asString();
    const Blob& data = input.parameters.at(INPUT_DATA).asBlob();

    std::ofstream str(fileName, std::ios::out | std::ios::trunc | std::ios::binary);

    str.write(data.data(), data.size());
}
}
