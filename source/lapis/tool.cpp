
#include "lapis/tool.hpp"

#include <unordered_map>

namespace lapis
{
const ObjectClass Tool::CLASS{Tool::CLASS_ID, nullptr};

Tool::~Tool() {}

const ObjectClass& Tool::getClass() const noexcept // override
{
    return CLASS;
}
} // namespace lapis
