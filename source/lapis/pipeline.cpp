
#include "lapis/pipeline.hpp"

#include <algorithm>
#include <cassert>

namespace lapis
{
const ObjectClass Pipeline::CLASS{Pipeline::CLASS_ID, nullptr};

void Pipeline::appendStep(Tool& tool, const PipelineStepInput& input /* = {} */)
{
    insertStepAt(steps_.size(), tool, input);
}

void Pipeline::insertStepAt(std::size_t index, Tool& tool,
                            const PipelineStepInput& input /* = {} */)
{
    PipelineStep step = {.tool = &tool, .input = input, .output = {}};
    validateStep(index, step);

    auto it = steps_.insert(steps_.begin() + index, std::move(step));

    // fix indices in subsequent steps
    for (; it != steps_.end(); ++it)
    {
        updateIndices(*it, index, 1);
    }
}

void Pipeline::removeStepAt(std::size_t index)
{
    if (index >= steps_.size())
    {
        throw std::invalid_argument("invalid step index");
    }

    auto it = steps_.erase(steps_.begin() + index);

    for (; it != steps_.end(); ++it)
    {
        updateIndices(*it, index, -1);
        removeStepReferences(*it, index);
    }
}

void Pipeline::execute(Context& context)
{
    prepareExecution();

    for (PipelineStep& step : steps_)
    {
        ToolInput input = generateInput(step);
        try
        {
            step.tool->execute(context, input, step.output);
        }
        catch (const PipelineExecutionException& ex)
        {
            throw ex;
        }
        catch (const std::exception& ex)
        {
            throw PipelineExecutionException(
                std::string(
                    "An unexpected exception occured during execution: ") +
                ex.what());
        }
    }
}

void Pipeline::prepareExecution()
{
    for (PipelineStep& step : steps_)
    {
        step.outputUsers.clear();
    }

    for (const PipelineStep& step : steps_)
    {
        for (const PipelineStepRelation& relation : step.input.relations)
        {
            PipelineStep& stepFrom = steps_[relation.stepFrom];

            // increase remaining users
            std::size_t& users = stepFrom.outputUsers[relation.nameFrom];
            ++users;
        }
    }
}

void Pipeline::validateStep(std::size_t index, const PipelineStep& step) const
{
    for (const PipelineStepRelation& relation : step.input.relations)
    {
        if (relation.stepFrom >= index)
        {
            throw InvalidStepException(
                "Step contains reference to non-previous step " +
                std::to_string(relation.stepFrom));
        }
    }
}

void Pipeline::updateIndices(PipelineStep& step, std::size_t fromIndex,
                             int offset) noexcept
{
    // update any indices >= fromIndex according to offset
    for (PipelineStepRelation& relation : step.input.relations)
    {
        if (relation.stepFrom >= fromIndex)
        {
            relation.stepFrom += offset;
        }
    }
}

void Pipeline::removeStepReferences(PipelineStep& step,
                                    std::size_t index) noexcept
{
    auto it =
        std::remove_if(step.input.relations.begin(), step.input.relations.end(),
                       [index](const PipelineStepRelation& relation) {
                           return relation.stepFrom == index;
                       });
    step.input.relations.erase(it, step.input.relations.end());
}

ToolInput Pipeline::generateInput(PipelineStep& step)
{
    ToolInput input;

    for (const PipelineStepConstant& constant : step.input.constants)
    {
        input.parameters[constant.name] = std::move(constant.value);
    }

    for (const PipelineStepRelation& relation : step.input.relations)
    {
        PipelineStep& stepFrom = steps_[relation.stepFrom];

        auto it = stepFrom.output.results.find(relation.nameFrom);
        if (it == stepFrom.output.results.end())
        {
            throw PipelineExecutionException(
                "Tool did not generate required output."); // TODO: detail
        }

        // decrease remaining users
        std::size_t& users = stepFrom.outputUsers.at(relation.nameFrom);
        assert(users > 0);
        --users;

        if (users == 0)
        {
            input.parameters[relation.nameTo] = std::move(it->second);
        }
        else
        {
            input.parameters[relation.nameTo] = it->second;
        }
    }

    return input;
}

Object* Pipeline::clone() const // override
{
    return new Pipeline(*this);
}

const ObjectClass& Pipeline::getClass() const noexcept // override
{
    return CLASS;
}
} // namespace lapis
