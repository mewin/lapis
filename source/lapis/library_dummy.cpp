
#include "lapis/library.hpp"

// stubs for library loading
// might allow compiling on unknown operating systems

namespace lapis
{
const char* Library::SHARED_LIBRARY_FILE_EXTENSION = "<none>";

Library* Library::loadFromSharedLibrary(const std::string& /* path */)
{
    throw LibraryException("dummy implementation");
}
}
