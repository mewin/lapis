
#include "lapis/object_class.hpp"

#include <stdexcept>

#include "lapis/object.hpp"

namespace lapis
{
ObjectClass::ObjectClass(const std::string& id, const Object* defaultObject,
        std::vector<MemberFunction> memberFunctions /* = {} */,
        std::vector<MemberProperty> memberProperties /* = {} */,
        const ObjectClass* baseClass /* = nullptr */)
    : id_(id),
    memberFunctions_(std::move(memberFunctions)),
    memberProperties_(std::move(memberProperties)),
    defaultObject_(defaultObject), baseClass_(baseClass)
{

}

Object* ObjectClass::newInstance() const
{
    if (defaultObject_ == nullptr)
    {
        throw std::runtime_error("Attempting to construct non-dynamic class: " + id_);
    }
    return defaultObject_->clone();
}
}
