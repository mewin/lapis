
#include "lapis/library.hpp"

#include <array>

#include <windows.h>

namespace lapis
{
const char* Library::SHARED_LIBRARY_FILE_EXTENSION = ".dll";

static std::string formatWindowsError()
{
    const DWORD error = GetLastError(); 
    std::array<char, 256> buffer;
    const DWORD res = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, nullptr, error,
                   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buffer.data(),
                   static_cast<DWORD>(buffer.size()), nullptr);
    if (res == 0)
    {
        return "Unknown error";
    }
    return buffer.data();
}

Library* Library::loadFromSharedLibrary(const std::string& path)
{
	HMODULE handle = LoadLibrary(path.c_str());
    if (handle == nullptr)
    {
        const std::string error = formatWindowsError();
        throw LibraryException(
            "Failed to open library: " + error);
    }

    FARPROC entryPoint = GetProcAddress(handle, ENTRY_POINT_NAME);
    if (entryPoint == nullptr)
    {
        FreeLibrary(handle);
        throw LibraryException("Missing entry point.");
    }

    Library* library = reinterpret_cast<entry_point_t>(entryPoint)();
    if (library == nullptr)
    {
        FreeLibrary(handle);
        throw LibraryException("Library initialization failed.");
    }

    return library;
}
}
