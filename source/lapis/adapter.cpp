
#include "lapis/adapter.hpp"

namespace lapis
{
const ObjectClass Adapter::CLASS{Adapter::CLASS_ID, nullptr};

Adapter::Adapter(const ObjectClass& objectClass, AdapterFlags flags)
    : objectClass_(objectClass), flags_(flags)
{

}

Adapter::~Adapter()
{
    
}

ObjectPtr<> Adapter::fromBlob(const Context& /* context */, const Blob& /* blob */,
        const std::string& /* fileName */) // virtual
{
    return nullptr;
}

Blob Adapter::toBlob(const Context& /* context */, const Object& /* object */,
        const std::string& /* fileName */) // virtual
{
    return {};
}

const ObjectClass& Adapter::getClass() const noexcept // override
{
    return CLASS;
}
}
