
#include "lapis/library.hpp"

#include <dlfcn.h>

// unix-specific code for library loading

namespace lapis
{
const char* Library::SHARED_LIBRARY_FILE_EXTENSION = ".so";

Library* Library::loadFromSharedLibrary(const std::string& path)
{
    void* handle = dlopen(path.c_str(), RTLD_NOW);
    if (handle == nullptr)
    {
        const char* error = dlerror();
        if (error == nullptr) {
            error = "unknown error";
        }
        throw LibraryException(
            std::string("Failed to open library: ") + error);
    }

    void* entryPoint = dlsym(handle, ENTRY_POINT_NAME);
    if (entryPoint == nullptr)
    {
        dlclose(handle);
        throw LibraryException("Missing entry point.");
    }

    Library* library = reinterpret_cast<entry_point_t>(entryPoint)();
    if (library == nullptr)
    {
        dlclose(handle);
        throw LibraryException("Library initialization failed.");
    }

    return library;
}
}
