
#include "lapis/types/bitmap.hpp"

namespace lapis::types
{
static const Bitmap DEFAULT_BITMAP;
const ObjectClass Bitmap::CLASS{Bitmap::CLASS_ID, &DEFAULT_BITMAP};

void Bitmap::create(unsigned width, unsigned height, PixelFormat pixelFormat /* = {} */)
{
    if (!validatePixelFormat(pixelFormat))
    {
        throw std::runtime_error("Invalid pixel format.");
    }

    width_ = width;
    pixelFormat_ = pixelFormat;
    data_.resize(width * height * pixelFormat.bytesPerChannel * pixelFormat.channels);
}

Value Bitmap::serialize() const // override
{
    Map serialized;

    serialized["width"] = static_cast<int>(width_);
    serialized["height"] = static_cast<int>(height());
    serialized["format_bytes_per_channel"] = static_cast<int>(pixelFormat_.bytesPerChannel);
    serialized["format_channels"] = static_cast<int>(pixelFormat_.channels);
    serialized["format_data_type"] = pixelDataTypeToString(pixelFormat_.dataType);
    serialized["data"] = data_;

    return serialized;
}

void Bitmap::deserialize(const Value& value) // override
{
    if (value.type() != ValueType::MAP)
    {
        throw SerializationException("Invalid value type, expected map.");
    }
    const Map& map = value.asMap();
    PixelFormat pixelFormat;
    unsigned width = static_cast<unsigned>(map.at("width").asInteger());
    unsigned height = static_cast<unsigned>(map.at("height").asInteger());
    pixelFormat.bytesPerChannel = static_cast<std::uint8_t>(map.at("format_bytes_per_channel").asInteger());
    pixelFormat.channels = static_cast<std::uint8_t>(map.at("format_channels").asInteger());
    pixelFormat.dataType = pixelDataTypeFromString(map.at("format_data_type").asString());
    
    if (!validatePixelFormat(pixelFormat))
    {
        throw SerializationException("Invalid pixel format.");
    }

    create(width, height, pixelFormat);

    const Blob& data = map.at("data").asBlob();
    if (data_.size() != data.size())
    {
        throw SerializationException("Image data size does not match specified image format.");
    }
    std::copy(data.begin(), data.end(), data_.begin());
}

Object* Bitmap::clone() const // override
{
    Bitmap* cloned = new Bitmap;
    cloned->create(width_, height(), pixelFormat_);
    std::copy(data_.begin(), data_.end(), cloned->data().begin());
    return cloned;
}

const ObjectClass& Bitmap::getClass() const noexcept // override
{
    return CLASS;
}

std::string pixelDataTypeToString(PixelDataType pixelDataType) noexcept
{
    switch(pixelDataType)
    {
        case PixelDataType::FLOAT:
            return "float";
        case PixelDataType::INTEGER:
            return "integer";
        case PixelDataType::UNSIGNED_INTEGER:
            return "unsigned_integer";
    }
    return "unsigned_integer";
}

PixelDataType pixelDataTypeFromString(const std::string& str)
{
    if (str == "float")
    {
        return PixelDataType::FLOAT;
    }
    else if (str == "integer")
    {
        return PixelDataType::INTEGER;
    }
    else if (str == "unsigned_integer")
    {
        return PixelDataType::UNSIGNED_INTEGER;
    }
    else
    {
        throw SerializationException("Invalid value for pixel data type: " + str);
    }
}

bool validatePixelFormat(const PixelFormat& pixelFormat) noexcept
{
    if (pixelFormat.channels < 1 || pixelFormat.channels > 4)
    {
        return false;
    }

    switch (pixelFormat.dataType)
    {
        case PixelDataType::FLOAT:
            if (pixelFormat.bytesPerChannel != 4 && pixelFormat.bytesPerChannel != 8)
            {
                return false;
            }
            break;
        case PixelDataType::INTEGER:
        case PixelDataType::UNSIGNED_INTEGER:
            if (pixelFormat.bytesPerChannel != 1 && pixelFormat.bytesPerChannel != 2
                && pixelFormat.bytesPerChannel != 4 && pixelFormat.bytesPerChannel != 8)
            {
                return false;
            }
            break;
        default:
            return false; // something went totally wrong
    }
    return true;
}
}
