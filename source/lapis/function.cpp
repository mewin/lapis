
#include "lapis/function.hpp"

namespace lapis
{
Function::~Function()
{

}

Value CppFunction::invoke(const std::vector<Value>& arguments) // override
{
    return wrapped_(arguments);
}
}
