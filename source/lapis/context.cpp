
#include "lapis/context.hpp"

#include <filesystem>
#include <fstream>
#include <unordered_set>
#include <queue>

#include "lapis/config.hpp"
#include "lapis/script.hpp"
#include "lapis/static_libraries.hpp"
#include "lapis/util/collections.hpp"
#include "lapis/util/io.hpp"
#include "lapis/util/graph.hpp"

namespace fs = std::filesystem;

namespace lapis
{
// helper structs for using converters and classes as a graph
struct ConverterUsage
{
    const Converter* converter = nullptr;
    bool inverted = false;

    ConverterUsage() = default;
    ConverterUsage(const ConverterUsage&) = default;
    constexpr ConverterUsage(const Converter* _converter, bool _inverted) noexcept 
        : converter(_converter), inverted(_inverted) {}
};

struct ConversionGraphTraits
{
    using node_t = const ObjectClass*;
    using edge_t = ConverterUsage;
    using cost_t = int;
};

template<>
struct Graph<ConversionGraphTraits>
{
private:
    const std::vector<std::unique_ptr<Converter>>& converters_;
public:
    explicit inline Graph(const std::vector<std::unique_ptr<Converter>>& converters) noexcept : converters_(converters) {};
public:
    const ObjectClass* edgeStart(const ConverterUsage& edge) const
    {
        return edge.inverted ? &edge.converter->classTo() : &edge.converter->classFrom();
    }
    const ObjectClass* edgeEnd(const ConverterUsage& edge) const
    {
        return edge.inverted ? &edge.converter->classFrom() : &edge.converter->classTo();
    }
    int edgeCost(const ConverterUsage& edge) const
    {
        return edge.converter->flags().canMoveConvert() ? 1 : 10;
    }
    void edgesFrom(const ObjectClass* node, std::vector<ConverterUsage>& result) const
    {
        for (const std::unique_ptr<Converter>& converter : converters_)
        {
            // is this converter applicable?
            if (&converter->classFrom() == node)
            {
                result.emplace_back(converter.get(), false);
            }
            else if (converter->flags().bidirectional() && &converter->classTo() == node)
            {
                result.emplace_back(converter.get(), true);
            }
        }
    }
};

const ObjectClass Context::CLASS{Context::CLASS_ID, nullptr};

void Context::loadAllLibraries()
{
    // TODO: make this configurable
    std::vector<std::string> folders = {
        "./libs"
    };
#if defined(_MSC_VER) && defined(_DEBUG)
    // TODO: remove this
    // temporary until configuration/environment variables are implemented
    folders.push_back("./libs/Debug");
#endif

    // load static libraries
    if constexpr (config::LINK_STATIC)
    {
        const std::vector<Library*>& staticLibraries = getStaticLibraries();
        libraries_.insert(libraries_.end(), staticLibraries.begin(), staticLibraries.end());
    }

    for (const std::string& folder : folders)
    {
        debug("Loading libraries from folder \"{0}\".", folder);

        std::vector<Library*> libraries = Library::loadAll(folder);
        for (Library* library : libraries)
        {
            debug("> Found \"{0}\".", library->id());
            libraries_.emplace_back(library);
        }
    }

    for (Library* library : libraries_)
    {
        verbose("Enabling library \"{0}\".", library->id());
        library->load(*this);
    }
}

Pipeline Context::importPipeline(std::istream& str, const std::string& fileName)
{
    Blob data = readWholeStreamBinary(str);
    std::shared_ptr<Pipeline> pipeline = objectFromBlob<Pipeline>(data,
            Pipeline::CLASS, fileName);

    if (pipeline == nullptr)
    {
        throw AdapterNotFoundException(fileName);
    }
    return std::move(*pipeline);
}

Pipeline Context::importPipelineFromFile(const std::string& fileName)
{
    // TODO: maybe switch to text mode depending on interface?
    std::ifstream str(fileName, std::ios::binary);

    if (str.fail())
    {
        throw std::runtime_error("Could not open file: " + fileName);
    }

    return importPipeline(str, fileName);
}


void Context::exportPipeline(const Pipeline& pipeline, std::ostream& str, const std::string& fileName)
{
    const Blob data = objectToBlob(pipeline, fileName);

    if (data.empty())
    {
        throw AdapterNotFoundException(fileName);
    }

    str.write(data.data(), data.size());

    if (!str)
    {
        throw std::runtime_error("Error writing pipeline to file: " + fileName);
    }
}

void Context::exportPipelineToFile(const Pipeline& pipeline, const std::string& fileName)
{
    // TODO: maybe switch to text mode depending on interface?
    std::ofstream str(fileName, std::ios::binary | std::ios::trunc);

    if (str.fail())
    {
        throw std::runtime_error("Could not open file: " + fileName);
    }

    exportPipeline(pipeline, str, fileName);
}

void Context::registerTool(std::unique_ptr<Tool>&& tool)
{
    if (tools_.find(tool->name()) != tools_.end())
    {
        throw DuplicateToolException(tool->name());
    }

    tools_.insert({tool->name(), std::move(tool)});
}

Tool* Context::findToolByName(const std::string& name) const
{
    auto it = tools_.find(name);

    if (it == tools_.end())
    {
        return nullptr;
    }

    return it->second.get();
}

void Context::registerAdapter(std::unique_ptr<Adapter>&& adapter)
{
    adapters_[&adapter->objectClass()].push_back(std::move(adapter));
}

std::vector<Adapter*> Context::findAdapters(const ObjectClass& objectClass,
        const std::string& fileName /* = "" */) const
{
    auto it = adapters_.find(&objectClass);
    
    if (it != adapters_.end())
    {
        std::vector<Adapter*> result = toRawPointers(it->second.begin(), it->second.end());

        // remove any adapters cannot be used
        if (!fileName.empty())
        {
            auto itRemove = std::remove_if(result.begin(), result.end(), [&](Adapter* adapter)
            {
                return !adapter->canHandle(*this, fileName);
            });
            result.erase(itRemove, result.end());
        }

        return result;
    }
    return {};
}

ObjectPtr<> Context::objectFromBlobImpl(const Blob& blob, const ObjectClass& objectClass,
        const std::string& fileName) const
{
    for (Adapter* adapter : findAdapters(objectClass, fileName))
    {
        ObjectPtr<> result = adapter->fromBlob(*this, blob, fileName);
        if (result != nullptr)
        {
            return result;
        }
    }

    return nullptr;
}

Blob Context::objectToBlob(const Object& object, const std::string& fileName)
{
    for (Adapter* adapter : findAdapters(object.getClass(), fileName))
    {
        Blob result = adapter->toBlob(*this, object, fileName);
        if (!result.empty())
        {
            return result;
        }
    }
    
    return {};
}

void Context::registerConverter(std::unique_ptr<Converter>&& converter)
{
    converters_.push_back(std::move(converter));
}

ObjectPtr<> Context::convertObjectImpl(const ObjectPtr<>& source, const ObjectClass& toClass)
{
    // Object conversion might required multiple steps.
    // Example: some.addon.BitmapImpl -> lapis.core.Bitmap -> other.addon.BitmapImpl
    //
    // We basically use Dijkstra's algorithm to find the shortest possible
    // conversion route. This also takes into account if a move-conversion can
    // take place as this is considerably cheaper.
    //
    // In our graph, the classes are nodes while the possible conversions are edges.

    const Graph<ConversionGraphTraits> graph(converters_);
    const GraphRoute route = shortestPath(graph, &source->getClass(), &toClass);

    // finally apply the conversions
    ObjectPtr<> result = source;
    for (const ConverterUsage& edge : route.edges)
    {
        result = edge.converter->convert(*result);
    }

    return result;
}

void Context::registerObjectClass(const ObjectClass& objectClass)
{
    auto it = objectClasses_.find(objectClass.id());
    if (it != objectClasses_.end())
    {
        if (it->second == &objectClass)
        {
            warning("ObjectClass {} is registered twice, ignoring.", objectClass.id());
            return;
        }
        throw DuplicateObjectClassException(objectClass.id());
    }
    objectClasses_.insert({objectClass.id(), &objectClass});
}

void Context::registerDefaultClasses()
{
    registerObjectClass(Adapter::CLASS);
    registerObjectClass(Context::CLASS);
    registerObjectClass(Converter::CLASS);
    registerObjectClass(Library::CLASS);
    registerObjectClass(Pipeline::CLASS);
    registerObjectClass(Script::CLASS);
    registerObjectClass(Tool::CLASS);
}

const ObjectClass& Context::getClass() const noexcept // override
{
    return CLASS;
}
}
