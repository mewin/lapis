
#include <algorithm>
#include <locale>
#include <type_traits>

#include "lapis/value.hpp"
#include "lapis/object.hpp"

namespace lapis
{
static const Vector EMPTY_VECTOR;
static const Map    EMPTY_MAP;
static const Blob   EMPTY_BLOB;

// buffer to store converted values in
static thread_local Value convertedValue;

const Value Value::EMPTY;

ValueType Value::type() const noexcept
{
    if (store_.valueless_by_exception()) {
        return ValueType::NONE;
    }
    
    return std::visit(
        [](auto&& arg)
        {
            using type_t = std::decay_t<decltype(arg)>;

            if constexpr (std::is_same_v<type_t, std::nullptr_t>) {
                return ValueType::NONE;
            }
            else if constexpr (std::is_same_v<type_t, bool>) {
                return ValueType::BOOLEAN;
            }
            else if constexpr (std::is_same_v<type_t, int>) {
                return ValueType::INTEGER;
            }
            else if constexpr (std::is_same_v<type_t, double>) {
                return ValueType::DOUBLE;
            }
            else if constexpr (std::is_same_v<type_t, std::string>) {
                return ValueType::STRING;
            }
            else if constexpr (std::is_same_v<type_t, ObjectPtr<>>) {
                return ValueType::OBJECT;
            }
            else if constexpr (std::is_same_v<type_t, Vector>) {
                return ValueType::VECTOR;
            }
            else if constexpr (std::is_same_v<type_t, Map>) {
                return ValueType::MAP;
            }
            else if constexpr (std::is_same_v<type_t, Blob>) {
                return ValueType::BLOB;
            }
            else if constexpr (std::is_same_v<type_t, FunctionPtr>) {
                return ValueType::FUNCTION;
            }
            else {
                static_assert(util::always_false_v<type_t>, "Non-exhaustive visitor!");
            }
        },
        store_);
}

bool Value::asBool(bool fallback /* = false */) const noexcept
{
    if (store_.valueless_by_exception()) {
        return fallback;
    }

    return std::visit(
        [fallback](auto&& arg) {
            using type_t = std::decay_t<decltype(arg)>;

            if constexpr (std::is_same_v<type_t, bool>)
            {
                return arg;
            }
            else if constexpr (std::is_same_v<type_t, int>)
            {
                return arg != 0;
            }
            else if constexpr (std::is_same_v<type_t, double>)
            {
                return arg != 0.0;
            }
            else if constexpr (std::is_same_v<type_t, std::string>)
            {
                std::string lower(arg);
                std::transform(lower.begin(), lower.end(), lower.begin(), [](char c) {
                    return std::tolower(c, std::locale());
                });

                return lower == "1" || lower == "t" || lower == "true";
            }
            else
            {
                return fallback;
            }
        },
        store_);
}

int Value::asInteger(int fallback /* = 0 */) const noexcept
{
    if (store_.valueless_by_exception()) {
        return fallback;
    }

    return std::visit(
        [fallback](auto&& arg) {
            using type_t = std::decay_t<decltype(arg)>;

            if constexpr (std::is_same_v<type_t, bool>)
            {
                return arg ? 1 : 0;
            }
            else if constexpr (std::is_same_v<type_t, int>)
            {
                return arg;
            }
            else if constexpr (std::is_same_v<type_t, double>)
            {
                return static_cast<int>(arg);
            }
            else if constexpr (std::is_same_v<type_t, std::string>)
            {
                try
                {
                    return std::stoi(arg);
                }
                catch(const std::logic_error&)
                {
                    return fallback;
                }
            }
            else
            {
                return fallback;
            }
        },
        store_);
}

double Value::asDouble(double fallback /* = 0.0 */) const noexcept
{
    if (store_.valueless_by_exception()) {
        return fallback;
    }

    return std::visit(
        [fallback](auto&& arg) {
            using type_t = std::decay_t<decltype(arg)>;

            if constexpr (std::is_same_v<type_t, bool>)
            {
                return arg ? 1.0 : 0.0;
            }
            else if constexpr (std::is_same_v<type_t, int>)
            {
                return static_cast<double>(arg);
            }
            else if constexpr (std::is_same_v<type_t, double>)
            {
                return arg;
            }
            else if constexpr (std::is_same_v<type_t, std::string>)
            {
                try
                {
                    return std::stod(arg);
                }
                catch(const std::logic_error&)
                {
                    return fallback;
                }
            }
            else
            {
                return fallback;
            }
        },
        store_);
}

std::string
Value::asString(const std::string& fallback /* = ""*/) const noexcept
{
    if (store_.valueless_by_exception()) {
        return fallback;
    }

    return std::visit(
        [fallback](auto&& arg) {
            using type_t = std::decay_t<decltype(arg)>;

            if constexpr (std::is_same_v<type_t, bool>)
            {
                return std::string(arg ? "true" : "false");
            }
            else if constexpr (std::is_same_v<type_t, int>)
            {
                return std::to_string(arg);
            }
            else if constexpr (std::is_same_v<type_t, double>)
            {
                return std::to_string(arg);
            }
            else if constexpr (std::is_same_v<type_t, std::string>)
            {
                return arg;
            }
            else if constexpr (std::is_same_v<type_t, ObjectPtr<>>)
            {
                return arg ? arg->toString() : "null";
            }
            else
            {
                // TODO: vector and map
                return fallback;
            }
        },
        store_);
}

ObjectPtr<> Value::asObjectImpl() const noexcept
{
    if (store_.valueless_by_exception()) {
        return nullptr;
    }

    if (std::holds_alternative<ObjectPtr<>>(store_))
    {
        return std::get<ObjectPtr<>>(store_);
    }
    return nullptr;
}

const Vector& Value::asVector(bool convert /* = false */) const noexcept
{
    if (std::holds_alternative<Vector>(store_))
    {
        return std::get<Vector>(store_);
    }
    
    if (!convert)
    {
        return EMPTY_VECTOR;
    }

    Vector converted;
    if (std::holds_alternative<Map>(store_))
    {
        const Map& map = std::get<Map>(store_);
        int startIndex = map.contains("0") ? 0 : 1; // allow 1-based arrays (e.g. from Lua)

        for (int i = startIndex; ; ++i)
        {
            auto it = map.find(std::to_string(i));
            if (it == map.end())
            {
                break;
            }
            converted.push_back(it->second);
        }
    }
    else
    {
        converted.push_back(*this);
    }
    
    convertedValue = converted;
    return convertedValue.asVector();
}

const Map& Value::asMap() const noexcept
{
    if (std::holds_alternative<Map>(store_))
    {
        return std::get<Map>(store_);
    }
    return EMPTY_MAP;
}

const Blob& Value::asBlob() const noexcept
{
    if (std::holds_alternative<Blob>(store_))
    {
        return std::get<Blob>(store_);
    }
    return EMPTY_BLOB;
}

FunctionPtr Value::asFunction() const noexcept
{
    if (std::holds_alternative<FunctionPtr>(store_))
    {
        return std::get<FunctionPtr>(store_);
    }
    return nullptr;
}
} // namespace lapis
