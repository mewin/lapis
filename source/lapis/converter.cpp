
#include "lapis/converter.hpp"

namespace lapis
{
const ObjectClass Converter::CLASS{CLASS_ID, nullptr};

Converter::Converter(const ObjectClass& classFrom, const ObjectClass& classTo,
        const ConverterFlags& flags /* = {} */) : classFrom_(classFrom),
        classTo_(classTo), flags_(flags)
{

}

Converter::~Converter()
{

}

ObjectPtr<> Converter::convert(Object&& source) const
{
    return convert(static_cast<const Object&>(source));
}

const ObjectClass& Converter::getClass() const noexcept // override
{
    return CLASS;
}
}
