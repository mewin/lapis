
#include "lapis/logging.hpp"

#include "lapis/config.hpp"

#include <iostream>

namespace lapis
{
LoggerMixin::LoggerMixin() noexcept
{
    if constexpr (config::DEBUG_BUILD)
    {
        minLogLevel_ = LogLevel::DEBUG;
    }
}

void LoggerMixin::log(LogLevel level, const std::string& message) const noexcept
{
    if (static_cast<int>(level) < static_cast<int>(minLogLevel_)) {
        return;
    }

    loggingFunction_(level, message);
    
}

void defaultLoggingFunction(LogLevel level, const std::string& message) noexcept
{
    bool error = static_cast<int>(level) >= static_cast<int>(LogLevel::WARNING);
    std::ostream& str = error ? std::cerr : std::cout;

    str << "[" << logLevelPrefix(level) << "] " << message << std::endl;
}
}