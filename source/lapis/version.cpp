
#include "lapis/version.hpp"

#include <fmt/format.h>
#include "lapis/config.hpp"

namespace lapis
{
const Version     LIBRARY_VERSION = lapis::config::VERSION;
const std::string LIBRARY_VERSION_STRING = versionString(lapis::config::VERSION);


std::string versionString(const Version& version) noexcept
{
    return fmt::format("{0}.{1}.{2}", version.major, version.minor, version.patch);
}
}
