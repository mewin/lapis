
#include "lapis/library.hpp"

#include <filesystem>
#include <iostream>

#include "lapis/config.hpp"
#include "lapis/static_libraries.inl"

namespace fs = std::filesystem;

namespace lapis
{
const ObjectClass Library::CLASS{Library::CLASS_ID, nullptr};

Library::Library(const std::string& id, const Version& version)
    : id_(id), version_(version)
{

}

Library ::~Library()
{

}

void Library::unload() {}

std::vector<Library*> Library::loadAll(const std::string& folder)
{
    // TODO: library loaders (with registry etc)?
    if (!fs::is_directory(folder)) {
        return {};
    }

    std::vector<Library*> result;

    for (const fs::directory_entry& entry : fs::directory_iterator(folder))
    {
        std::string extension = entry.path().extension().string();

        if (extension == SHARED_LIBRARY_FILE_EXTENSION)
        {
            try
            {
                Library* library =
                    Library::loadFromSharedLibrary(entry.path().string());
                result.push_back(library);
            }
            catch(LibraryException& ex)
            {
                std::cerr << ex.what() << std::endl; // TODO: logging
            }
        }
    }

    return result;
}

const ObjectClass& Library::getClass() const noexcept // override
{
    return CLASS;
}

// library list implementation
std::vector<Library*>& getStaticLibraries()
{
    static std::vector<Library*> libraries;
    if constexpr (config::LINK_STATIC)
    {
        if (libraries.empty())
        {
            initStaticLibraries(libraries);
        }
    }
    return libraries;
}
void registerStaticLibrary(Library* library)
{
    getStaticLibraries().push_back(library);
}
}
