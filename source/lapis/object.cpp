
#include "lapis/object.hpp"

#include <stdexcept>

#include <fmt/format.h>

namespace lapis
{
Object::~Object() {}

std::string Object::toString() const noexcept
{
    return fmt::format("[{}@{}]", getClass().id(), static_cast<const void*>(this));
}

Object* Object::clone() const
{
    throw NotClonableException(getClass().id());
}
} // namespace lapis
