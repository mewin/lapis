
#include "lapis/script.hpp"

namespace lapis
{
const ObjectClass Script::CLASS{Script::CLASS_ID, nullptr};

Script::~Script()
{
    
}

void Script::initApi(const Context& context)
{
    for (const auto& entry : context.objectClasses())
    {
        registerObjectClass(*entry.second);
    }
}

const ObjectClass& Script::getClass() const noexcept // override
{
    return CLASS;
}
}