
#include <string>

#include "lapis/context.hpp"
#include "lapis/library.hpp"
#include "lapis/version.hpp"
#include "lapis/util/macros.hpp"

#include "lapis_lib_bitmap_io/adapter_bitmap_png.hpp"

namespace lapis::libbitmapio
{
class LibBitmapIO : public Library
{
public:
    static const constexpr auto           ID      = "lapis.bitmap_io";
    static const constexpr lapis::Version VERSION = { 0, 0, 1 };
public:
    inline LibBitmapIO() : Library(ID, VERSION) {}
public: // interface
    void load(Context& context) override;
};

void LibBitmapIO::load(Context& context)
{
    context.registerAdapter(new AdapterBitmapPNG());
}
}

LAPIS_LIBRARY_ENTRY_POINT(bitmap_io)
{
    
    return new lapis::libbitmapio::LibBitmapIO();
}
