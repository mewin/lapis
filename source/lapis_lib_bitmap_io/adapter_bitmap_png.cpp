
#include "lapis_lib_bitmap_io/adapter_bitmap_png.hpp"

#include <cassert>
#include <csetjmp>
#include <cstring>
#include <memory>
#include <utility>

#include <png.h>

#include "lapis/context.hpp"
#include "lapis/util/macros.hpp"
#include "lapis_lib_bitmap_io/png_pointers.hpp"

#pragma warning(disable: 4611) // setjmp

namespace lapis::libbitmapio
{
static const constexpr std::size_t PNG_SIGNATURE_LENGTH = 8;

static void pngErrorCallback(png_structp pngStruct, png_const_charp message)
{
    (void) pngStruct;
    (void) message;
}

static void pngWarningCallback(png_structp pngStruct, png_const_charp message)
{
    (void) pngStruct;
    (void) message;
}

struct PNGReadPayload
{
    const Blob& blob;
    std::size_t position = 0;
};

struct PNGWritePayload
{
    Blob& blob;
};

static void pngReadCallback(png_structp pngStruct, png_bytep data, png_size_t length)
{
    PNGReadPayload& payload = *static_cast<PNGReadPayload*>(png_get_io_ptr(pngStruct));

    if (length > payload.blob.size() - payload.position)
    {
        png_error(pngStruct, "Unexpected end of data.");
        return;
    }

    std::memcpy(data, &payload.blob[payload.position], length);
    payload.position += length;
}

static void pngWriteCallback(png_structp pngStruct, png_bytep data, png_size_t length)
{
    PNGWritePayload& payload = *static_cast<PNGWritePayload*>(png_get_io_ptr(pngStruct));
    const std::span<const char> cdata = {reinterpret_cast<const char*>(data), length};
    payload.blob.insert(payload.blob.end(), cdata.begin(), cdata.end());
}

static void pngFlushCallback(png_structp /* pngStruct */)
{

}

static inline PNGPointers initLibPNG(const Context& context, PNGPointers::Mode mode)
{
    // TODO: custom memory allocation (when lapis supports it)
    PNGPointers result;
    result.mode = mode;

    if (mode == PNGPointers::Mode::READ)
    {
        result.pngStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr,
                &pngErrorCallback, &pngWarningCallback);
    }
    else
    {
        result.pngStruct = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr,
                &pngErrorCallback, &pngWarningCallback);
    }

    if (result.pngStruct == nullptr)
    {
        context.warning("Error creating libpng png_struct.");
        return {};
    }

    result.infoStruct = png_create_info_struct(result.pngStruct);
    if (result.infoStruct == nullptr)
    {
        context.warning("Error creating libpng info_struct.");
        return {};
    }

    return result;
}

static inline bool setupPNGPixelFormat(const Context& context, PNGPointers& pngPointers, types::PixelFormat& pixelFormat)
{
    const png_byte colorType = png_get_color_type(pngPointers.pngStruct, pngPointers.infoStruct);
    const png_byte bitDepth = png_get_bit_depth(pngPointers.pngStruct, pngPointers.infoStruct);

    switch (colorType)
    {
        case PNG_COLOR_TYPE_GRAY:
            pixelFormat.channels = 1;
            if (bitDepth < 8)
            {
                png_set_expand_gray_1_2_4_to_8(pngPointers.pngStruct);
            }
            break;
        case PNG_COLOR_TYPE_GRAY_ALPHA:
            // gray + alpha is not supported, expand to rgb
            png_set_gray_to_rgb(pngPointers.pngStruct);
            pixelFormat.channels = 4;
            break;
        case PNG_COLOR_TYPE_RGB:
            pixelFormat.channels = 3;
            break;
        case PNG_COLOR_TYPE_RGBA:
            pixelFormat.channels = 4;
            break;
        case PNG_COLOR_TYPE_PALETTE:
            png_set_palette_to_rgb(pngPointers.pngStruct);
            pixelFormat.channels = 3;
            break;
        default:
            context.warning("Unsupported PNG color type.");
            return false;
    }

    // transform tRNS struct to alpha channel
    if (png_get_valid(pngPointers.pngStruct, pngPointers.infoStruct, PNG_INFO_tRNS))
    {
        png_set_tRNS_to_alpha(pngPointers.pngStruct);
        pixelFormat.channels = 4;
    }

    // support 16 bit colors
    if (bitDepth == 16)
    {
        pixelFormat.bytesPerChannel = 2;
    }

    return true;
}

AdapterBitmapPNG::AdapterBitmapPNG(): Adapter(
    types::Bitmap::CLASS,
    AdapterFlags().canImport(true).canExport(true)
)
{

}

bool AdapterBitmapPNG::canHandle(const Context& /* context */,
        const std::string& fileName) const noexcept // override
{
    return fileName.empty() || fileName.ends_with(".png");
}

ObjectPtr<> AdapterBitmapPNG::fromBlob(const Context& context, const Blob& blob,
        const std::string& /* fileName */) // override
{
    if (blob.size() < PNG_SIGNATURE_LENGTH)
    {
        return nullptr;
    }

    // shortcut for libpng
    const std::span<const png_byte> data = {reinterpret_cast<const png_byte*>(
        blob.data()), blob.size()};

    // check png signature
    if (png_sig_cmp(data.data(), 0, PNG_SIGNATURE_LENGTH) != 0)
    {
        return nullptr;
    }

    // init libpng
    PNGPointers pngPointers = initLibPNG(context, PNGPointers::Mode::READ);
    if (pngPointers.pngStruct == nullptr)
    {
        // error was already reported by initLibPNG
        return nullptr;
    }

    // Make sure any allocated data will be released when png jumps back here.
    // Anything that dynamically allocates memory needs to be declared here to make
    // sure the destructor is called in case of a longjump by libpng.
    ObjectPtr<types::Bitmap> bitmap; // TODO: use custom pointer type for custom allocation?
    std::vector<png_bytep> rowPointers;

    // handle errors
    if (setjmp(png_jmpbuf(pngPointers.pngStruct)))
    {
        return nullptr;
    }

    // setup reading from our blob
    PNGReadPayload payload = {blob};
    png_set_read_fn(pngPointers.pngStruct, &payload, &pngReadCallback);
    
    // read image header
    png_read_info(pngPointers.pngStruct, pngPointers.infoStruct);

    // detect optimal pixel format
    types::PixelFormat pixelFormat;
    if (!setupPNGPixelFormat(context, pngPointers, pixelFormat))
    {
        return nullptr;
    }

    // setup result image
    const png_uint_32 width = png_get_image_width(pngPointers.pngStruct, pngPointers.infoStruct);
    const png_uint_32 height = png_get_image_width(pngPointers.pngStruct, pngPointers.infoStruct);
    bitmap = std::make_unique<types::Bitmap>(); // TODO: use custom allocation callback?

    bitmap->create(width, height, pixelFormat);

    // read into our bitmap
    rowPointers.resize(height);
    
    for (png_uint_32 y = 0; y < height; ++y)
    {
        rowPointers[y] = reinterpret_cast<png_bytep>(
                &bitmap->data()[y * bitmap->bytesPerRow()]);
    }

    // actually read the image
    png_read_image(pngPointers.pngStruct, rowPointers.data());

    // // read end
    // png_read_end(pngPointers.pngStruct, pngPointers.endStruct);

    return bitmap;
}

Blob AdapterBitmapPNG::toBlob(const Context& context, const Object& object,
        const std::string& /* fileName */) // override
{
    const types::Bitmap& bitmap = dynamic_cast<const types::Bitmap&>(object);

    // check if bitmap is compatible
    if (!checkFormatCompatible(bitmap.pixelFormat()))
    {
        // TODO: convert bitmap to compatible format
        return {};
    }

    // init libpng
    PNGPointers pngPointers = initLibPNG(context, PNGPointers::Mode::WRITE);
    if (pngPointers.pngStruct == nullptr)
    {
        // error was already reported by initLibPNG
        return {};
    }

    // define anything that has to be deleted before setjmp
    Blob blob;
    std::vector<const png_byte*> rowPointers;

    // handle errors
    if (setjmp(png_jmpbuf(pngPointers.pngStruct)))
    {
        return {};
    }

    // setup writing into the blob
    PNGWritePayload payload{blob};
    png_set_write_fn(pngPointers.pngStruct, &payload, &pngWriteCallback, &pngFlushCallback);

    // set header
    const types::PixelFormat& format = bitmap.pixelFormat();
    png_set_IHDR(pngPointers.pngStruct, pngPointers.infoStruct,
            bitmap.width(), bitmap.height(), // width, height
            8 * format.bytesPerChannel,      // bit depth
            format.channels == 3 ? PNG_COLOR_TYPE_RGB : PNG_COLOR_TYPE_RGB_ALPHA, // color type
            PNG_INTERLACE_ADAM7,             // interlace type
            PNG_COMPRESSION_TYPE_DEFAULT,    // compression type
            PNG_FILTER_TYPE_DEFAULT          // filter method
    );

    // read from our bitmap
    rowPointers.resize(bitmap.height());
    
    for (png_uint_32 y = 0; y < bitmap.height(); ++y)
    {
        rowPointers[y] = reinterpret_cast<const png_byte*>(
                &bitmap.data()[y * bitmap.bytesPerRow()]);
    }
    png_set_rows(pngPointers.pngStruct, pngPointers.infoStruct,
            const_cast<png_bytepp>(rowPointers.data()));

    // write the image
    png_write_png(pngPointers.pngStruct, pngPointers.infoStruct,
            PNG_TRANSFORM_IDENTITY, nullptr);
    
    return blob;
}

bool AdapterBitmapPNG::checkFormatCompatible(const types::PixelFormat& format) const noexcept
{
    // only 3 or 4 channels
    if (format.channels != 3 && format.channels != 4)
    {
        return false;
    }

    // only uint
    if (format.dataType != types::PixelDataType::UNSIGNED_INTEGER)
    {
        return false;
    }

    // only 8 or 16 bits per channel
    if (format.bytesPerChannel > 2)
    {
        return false;
    }

    return true;
}
}
