
#include "lapis_lib_json/adapter_pipeline_json.hpp"

#include <span>
#include <sstream>

#include "nlohmann/json.hpp"

#include "lapis/context.hpp"
#include "lapis/pipeline.hpp"
#include "lapis/value.hpp"
#include "lapis_lib_json/value_json.hpp"

namespace lapis
{
static thread_local const Context* context__ = nullptr; // unfortunatelly I dont see a better way to do this :/

void to_json(nlohmann::json& json, const PipelineStepRelation& pipelineStepRelation)
{
    json = nlohmann::json{
        {"name_from", pipelineStepRelation.nameFrom},
        {"name_to", pipelineStepRelation.nameTo},
        {"step_from", pipelineStepRelation.stepFrom}
    };
}

void from_json(const nlohmann::json& json, PipelineStepRelation& pipelineStepRelation)
{
    json.at("name_from").get_to(pipelineStepRelation.nameFrom);
    json.at("name_to").get_to(pipelineStepRelation.nameTo);
    json.at("step_from").get_to(pipelineStepRelation.stepFrom);
}

void to_json(nlohmann::json& json, const PipelineStepConstant& pipelineStepConstant)
{
    json = nlohmann::json{
        {"name", pipelineStepConstant.name},
        {"value", pipelineStepConstant.value}
    };
}

void from_json(const nlohmann::json& json, PipelineStepConstant& pipelineStepConstant)
{
    json.at("name").get_to(pipelineStepConstant.name);
    json.at("value").get_to(pipelineStepConstant.value);
}

void to_json(nlohmann::json& json, const PipelineStepInput& pipelineStepInput)
{
    json = nlohmann::json{
        {"relations", pipelineStepInput.relations},
        {"constants", pipelineStepInput.constants}
    };
}

void from_json(const nlohmann::json& json, PipelineStepInput& pipelineStepInput)
{
    json.at("relations").get_to(pipelineStepInput.relations);
    json.at("constants").get_to(pipelineStepInput.constants);
}

void to_json(nlohmann::json& json, const PipelineStep& serializedPipelineStep)
{
    json = nlohmann::json{
        {"tool", serializedPipelineStep.tool->name()},
        {"input", serializedPipelineStep.input}
    };
}

void to_json(nlohmann::json& json, const Pipeline& serializedPipeline)
{
    json = nlohmann::json{
        {"steps", serializedPipeline.steps()}
    };
}

void from_json(const nlohmann::json& json, Pipeline& pipeline)
{
    for (const nlohmann::json& stepJson : json.at("steps"))
    {
        std::string toolName;
        PipelineStepInput stepInput;
        stepJson.at("tool").get_to(toolName);
        stepJson.at("input").get_to(stepInput);

        Tool* tool = context__->findToolByName(toolName);
        if (tool == nullptr)
        {
            throw ToolNotFoundException(toolName);
        }
        pipeline.appendStep(*tool, stepInput);
    }
}
}

namespace lapis::libjson
{
AdapterPipelineJson::AdapterPipelineJson() : Adapter(
    Pipeline::CLASS,
    AdapterFlags().canImport(true).canExport(true)
)
{

}

bool AdapterPipelineJson::canHandle(const Context& /* context */, const std::string& fileName) const noexcept // override
{
    return fileName.ends_with(".json") || fileName.ends_with(".ljp");
}

ObjectPtr<> AdapterPipelineJson::fromBlob(const Context& context,
        const Blob& blob, const std::string& /* fileName */) // override
{
    context__ = &context;

    nlohmann::json json;
    const std::string text(blob.begin(), blob.end());
    std::istringstream stream(text);
    stream >> json;

    Pipeline pipeline = json.get<Pipeline>();
    return std::make_shared<Pipeline>(std::move(pipeline));
}

Blob AdapterPipelineJson::toBlob(const Context& /* context */,
        const Object& object, const std::string& /* fileName */) // override
{
    const Pipeline& pipeline = static_cast<const Pipeline&>(object);
    nlohmann::json json;
    std::ostringstream stream;
    json = pipeline;
    stream << json;
    
    const std::string text = stream.str();
    return Blob(text.begin(), text.end());
}
}
