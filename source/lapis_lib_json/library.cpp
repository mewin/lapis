
#include <string>

#include "lapis/context.hpp"
#include "lapis/library.hpp"
#include "lapis/version.hpp"
#include "lapis/util/macros.hpp"

#include "lapis_lib_json/adapter_pipeline_json.hpp"
#include "lapis_lib_json/tools_json.hpp"

namespace lapis::libjson
{
class LibJson : public Library
{
public:
    static const constexpr auto           ID      = "lapis.json";
    static const constexpr lapis::Version VERSION = { 0, 0, 1 };
public:
    inline LibJson() : Library(ID, VERSION) {}
public: // interface
    void load(Context& context) override;
};

void LibJson::load(Context& context)
{
    context.registerAdapter(new AdapterPipelineJson());
    context.registerTool(new ToolParseJson());
    context.registerTool(new ToolGenerateJson());
}
}

LAPIS_LIBRARY_ENTRY_POINT(json)
{
    return new lapis::libjson::LibJson();
}
