
#include "lapis_lib_json/value_json.hpp"

#include <stdexcept>

namespace lapis
{
void to_json(nlohmann::json& json, const Value& value)
{
    switch (value.type())
    {
    case ValueType::NONE:
        json = nlohmann::json();
        break;
    case ValueType::BOOLEAN:
        json = value.asBool();
        break;
    case ValueType::INTEGER:
        json = value.asInteger();
        break;
    case ValueType::DOUBLE:
        json = value.asDouble();
        break;
    case ValueType::STRING:
        json = value.asString();
        break;
    case ValueType::OBJECT:
        throw std::runtime_error("Cannot serialize objects (yet)");
    case ValueType::VECTOR:
        json = value.asVector();
        break;
    case ValueType::MAP:
        json = value.asMap();
        break;
    case ValueType::FUNCTION:
    case ValueType::BLOB:
    case ValueType::ANY:
        throw std::runtime_error("Invalid return value from Value::type()");
    }
}
void from_json(const nlohmann::json& json, Value& value)
{
    if (json.is_null()) {
        value = Value();
    }
    else if (json.is_boolean()) {
        value = json.get<bool>();
    }
    else if (json.is_number_integer()) {
        value = json.get<int>();
    }
    else if (json.is_number()) {
        value = json.get<double>();
    }
    else if (json.is_string()) {
        value = json.get<std::string>();
    }
    else if (json.is_array()) {
        value = json.get<Vector>();
    }
    else if (json.is_object()) {
        value = json.get<Map>();
    }
    else {
        throw std::runtime_error(std::string("Unsupported type in json: ") + json.type_name());
    }
}
}
