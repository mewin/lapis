
#include "lapis_lib_json/tools_json.hpp"

#include <exception>

#include "nlohmann/json.hpp"

#include "lapis/pipeline.hpp"

#include "lapis_lib_json/value_json.hpp"

namespace lapis::libjson
{
void ToolParseJson::execute(Context& /* context */, const ToolInput& input, ToolOutput& output)
{
    const std::string& text = input.parameters.at(INPUT_JSON).asString();
    try
    {
        nlohmann::json json = nlohmann::json::parse(text);
        output.results[OUTPUT_VALUE] = json;
    }
    catch(const std::exception& ex)
    {
        throw PipelineExecutionException(ex.what());
    }
}

void ToolGenerateJson::execute(Context& /* context */, const ToolInput& input, ToolOutput& output)
{
    try
    {
        nlohmann::json json = input.parameters.at(INPUT_VALUE);
        output.results[OUTPUT_JSON] = json.dump();
    }
    catch(const std::exception& ex)
    {
        throw PipelineExecutionException(ex.what());
    }
}
}
