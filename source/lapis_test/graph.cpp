
#include "catch.hpp"

#include <vector>

#include "lapis/util/graph.hpp"

namespace lapis
{
struct TestGraphEdge
{
    int start;
    int end;
    int cost;
};

struct TestGraphData
{
    std::vector<int> nodes;
    std::vector<TestGraphEdge> edges;
};

struct TestGraphTraits
{
    using node_t = int;
    using edge_t = TestGraphEdge;
    using cost_t = int;
};

template<>
struct Graph<TestGraphTraits>
{
private:
    const TestGraphData data_;
public:
    explicit inline Graph(TestGraphData data) noexcept : data_(std::move(data)) {};
public:
    int edgeStart(const TestGraphEdge& edge) const
    {
        return edge.start;
    }
    int edgeEnd(const TestGraphEdge& edge) const
    {
        return edge.end;
    }
    int edgeCost(const TestGraphEdge& edge) const
    {
        return edge.cost;
    }
    void edgesFrom(int node, std::vector<TestGraphEdge>& result) const
    {
        for (const TestGraphEdge& edge : data_.edges)
        {
            if (edge.start == node)
            {
                result.push_back(edge);
            }
        }
    }
};

TEST_CASE("shortestPath", "[graph]")
{
    TestGraphData graphData;

    // This is our (pretty simple) testing graph here:
    // (1) --1-> (2) --1-> (3) --2-> (4)       (5)
    //  \<---1---/ \--------4------->/

    graphData.nodes = {1, 2, 3, 4};
    graphData.edges = {
        // from, to, cost
        {1, 2, 1},
        {2, 1, 1},
        {2, 4, 4},
        {2, 3, 1},
        {3, 4, 2}
    };

    Graph<TestGraphTraits> graph(graphData);
    SECTION("Find correct existing route")
    {
        GraphRoute result = shortestPath(graph, 1, 4);

        REQUIRE(result.edges.size() == 3);
        REQUIRE(result.edges[0].start == 1);
        REQUIRE(result.edges[0].end == 2);
        REQUIRE(result.edges[1].start == 2);
        REQUIRE(result.edges[1].end == 3);
        REQUIRE(result.edges[2].start == 3);
        REQUIRE(result.edges[2].end == 4);
        REQUIRE(result.cost == 4);
    }

    SECTION("Find empty route to same node.")
    {
        GraphRoute result = shortestPath(graph, 3, 3);
        REQUIRE(result.edges.size() == 0);
    }

    SECTION("Do not find route to decoupled node.")
    {
        GraphRoute result = shortestPath(graph, 1, 5);
        REQUIRE(result.edges.size() == 0);
    }
}
}
