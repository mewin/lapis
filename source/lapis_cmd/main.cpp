
#include <iostream>
#include <stdexcept>

#include "lapis_cmd/application.hpp"
#include "lapis_cmd/options.hpp"

enum
{
    EXIT_CODE_SUCCESS           = 0,
    EXIT_CODE_INVALID_OPTION    = -1,
    EXIT_CODE_PIPELINE_ERROR    = -2,
    EXIT_CODE_APPLICATION_ERROR = -3
};

int main(int argc, char* argv[])
{
    using namespace lapis::cmd;

    ApplicationOptions options;
    try
    {
        parseCommandLine(argc, argv, options);
    }
    catch(std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
        printUsage();
        return EXIT_CODE_INVALID_OPTION;
    }

    switch (options.operationMode)
    {
    case OperationMode::HELP:
        printHelp();
        return EXIT_CODE_SUCCESS;
    case OperationMode::VERSION:
        printVersion();
        return EXIT_CODE_SUCCESS;
    case OperationMode::NONE:
        printHelp();
        break;
    default:
        try
        {
            Application application;
            application.run(options);
        }
        catch(const InvalidOptionsException& ex)
        {
            std::cerr << ex.what() << std::endl;
            printUsage();
            return EXIT_CODE_INVALID_OPTION;
        }
        catch(const std::runtime_error& ex)
        {
            std::cerr << "Error during pipeline execution: \n"
                << ex.what() << std::endl;
            return EXIT_CODE_PIPELINE_ERROR;
        }
        catch(const std::exception& ex)
        {
            std::cerr << "Unexpected exception during pipeline execution: \n"
                << ex.what() << std::endl;
            return EXIT_CODE_APPLICATION_ERROR;
        }
    }

    return EXIT_CODE_SUCCESS;
}
