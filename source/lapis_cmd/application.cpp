
#include "lapis_cmd/application.hpp"

#include "lapis/context.hpp"

namespace lapis::cmd
{
void Application::run(const ApplicationOptions& options)
{
    checkOptions(options);

    context.registerDefaultClasses();
    context.loadAllLibraries();
    
    switch (options.operationMode)
    {
        case OperationMode::RUN:
            runModeRun(std::get<OperationModeRunArguments>(
                options.operationModeOptions));
            break;
        case OperationMode::CONVERT:
            runModeConvert(std::get<OperationModeConvertArguments>(
                options.operationModeOptions));
        default:
            break;
    }
}

void Application::runModeRun(const OperationModeRunArguments& args)
{
    Pipeline pipeline = context.importPipelineFromFile(args.fileName);

    context.arguments() = args.pipelineArguments;
    pipeline.execute(context);
}

void Application::runModeConvert(const OperationModeConvertArguments& args)
{
    context.info("Converting \"{}\" to \"{}\".", args.fileNameFrom, args.fileNameTo);
    Pipeline pipeline = context.importPipelineFromFile(args.fileNameFrom);
    context.exportPipelineToFile(pipeline, args.fileNameTo);
}

void Application::checkOptions(const ApplicationOptions& options)
{
    switch (options.operationMode)
    {
        case OperationMode::RUN:
        {
            const OperationModeRunArguments& args =
                std::get<OperationModeRunArguments>(options.operationModeOptions);
            if (args.fileName.empty())
            {
                throw InvalidOptionsException("Missing pipeline file parameter.");
            }
            break;
        }
        case OperationMode::CONVERT:
        {
            const OperationModeConvertArguments& args = 
                std::get<OperationModeConvertArguments>(options.operationModeOptions);
            if (args.fileNameFrom.empty())
            {
                throw InvalidOptionsException("Missing source file parameter.");
            }
            if (args.fileNameTo.empty())
            {
                throw InvalidOptionsException("Missing destination file parameter.");
            }
            break;
        }
        default:
            break;
    }
}
}
