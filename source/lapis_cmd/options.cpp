 
#include "lapis_cmd/options.hpp"

#include <iostream>
#include <vector>

#include "cxxopts/cxxopts.hpp"

#include "lapis/version.hpp"

namespace lapis::cmd
{
static const constexpr auto GROUP_COMMON    = "Common";
static const constexpr auto GROUP_OPERATION = "Operation";
static const cxxopts::Options& getCmdLineOptions()
{
    static cxxopts::Options options("lapis", "Command line utility for Lapis");
    static bool initialized = false;

    if (!initialized)
    {
        options.add_options(GROUP_COMMON)
            ("h,help",  "Prints full help.")
            ("version", "Prints version information.")
        ;

        options.add_options(GROUP_OPERATION)
            ("mode",       "Execution mode.", cxxopts::value<std::string>())
            ("args",       "More arguments.", cxxopts::value<std::vector<std::string>>())
            ("v,verbose",  "Print more information.")
        ;

        options.parse_positional({"mode", "args"});
        options.custom_help("<mode>");
        options.positional_help("<arguments...>");

        initialized = true;
    }

    return options;
}

template<typename TIter>
std::unordered_map<std::string, std::string> parsePipelineOptions(
    TIter first, TIter last)
{
    std::unordered_map<std::string, std::string> result;
    for (auto it = first; it != last; ++it)
    {
        const std::string& option = *it;
        std::size_t pos = option.find('=');
        if (pos == std::string::npos)
        {
            result[option] = "1";
        }
        else
        {
            std::string name = option.substr(0, pos);
            std::string value = option.substr(pos + 1);
            result[name] = value;
        }
    }
    return result;
}

OperationMode parseOperationMode(const std::string& name)
{
    if (name == "r" || name == "run")
    {
        return OperationMode::RUN;
    }
    else if (name == "c" || name == "convert")
    {
        return OperationMode::CONVERT;
    }
    throw std::runtime_error("Invalid operation mode. Valid values are: r(un),(c)onvert,(l)ist");
}

operation_mode_arguments_t parseOperationArguments(const std::vector<std::string>& args,
    OperationMode operationMode)
{
    switch (operationMode)
    {
    case OperationMode::RUN:
        return OperationModeRunArguments {
            .fileName          = args.empty() ? "" : args[0],
            .pipelineArguments = parsePipelineOptions(args.begin() + 1, args.end())
        };
    case OperationMode::CONVERT:
        return OperationModeConvertArguments {
            .fileNameFrom = args.empty() ? "" : args[0],
            .fileNameTo   = args.size() < 2 ? "" : args[1]
        };
    default:
        throw std::runtime_error("Invalid arguments.");
    }
}

void parseCommandLine(int argc, char* argv[], ApplicationOptions& optionsOut)
{
    cxxopts::Options options = getCmdLineOptions();
    cxxopts::ParseResult result = options.parse(argc, argv);

    if (result.count("help") > 0)
    {
        optionsOut.operationMode = OperationMode::HELP;
        return;
    }
    
    if (result.count("version") > 0)
    {
        optionsOut.operationMode = OperationMode::VERSION;
        return;
    }

    if (result.count("verbose") > 0)
    {
        optionsOut.verbose = true;
    }

    if (result.count("mode") > 0)
    {
        optionsOut.operationMode = parseOperationMode(result["mode"].as<std::string>());

        std::vector<std::string> argList;
        argList = result["args"].as<std::vector<std::string>>();
        optionsOut.operationModeOptions = parseOperationArguments(argList, optionsOut.operationMode);
    }
}

void printHelp() noexcept
{
    cxxopts::Options options = getCmdLineOptions();
    std::cout << options.help() << std::endl;
}

void printUsage() noexcept
{
    cxxopts::Options options = getCmdLineOptions();
    std::cout << options.help({GROUP_COMMON}) << std::endl; // only print "common" options
}

void printVersion() noexcept
{
    std::cout << "Lapis Version: " << lapis::LIBRARY_VERSION_STRING << std::endl;
}
}