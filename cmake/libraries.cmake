

# common macro for defining a library
# usage: add_lapis_library(name source0.cpp source1.cpp ...)
macro(add_lapis_library lib_name)
    if (${LAPIS_LINK_STATIC})
        add_library(lapis_lib_${lib_name} STATIC ${ARGN})
        target_compile_definitions(lapis_lib_${lib_name} PRIVATE "LAPIS_LINK_STATIC=1")

        target_link_libraries(lapis_lib_${lib_name} PRIVATE lapis)
        target_link_libraries(lapis PRIVATE lapis_lib_${lib_name})

        file(APPEND "${LAPIS_STATIC_LIB_LIST_FILE}" "\
    extern lapis::Library* initLibrary_${lib_name}();\n\
    libraries.push_back(initLibrary_${lib_name}());\n"
        )
    else()
        add_library(lapis_lib_${lib_name} SHARED ${ARGN})

        target_link_libraries(lapis_lib_${lib_name} PRIVATE lapis)
        set_target_properties(lapis_lib_${lib_name} PROPERTIES WINDOWS_EXPORT_ALL_SYMBOLS ON)

        set_target_properties(lapis_lib_${lib_name} PROPERTIES
            OUTPUT_NAME ${lib_name}
            LIBRARY_OUTPUT_DIRECTORY "libs"
            RUNTIME_OUTPUT_DIRECTORY "libs"
        )
    endif()

    target_include_directories(lapis_lib_${lib_name} PRIVATE "${CMAKE_SOURCE_DIR}/include/private")

    # high warning level
    target_compile_options(lapis_lib_${lib_name} PRIVATE $<$<C_COMPILER_ID:MSVC>:/W4 /WX>)
    target_compile_options(lapis_lib_${lib_name} PRIVATE $<$<NOT:$<C_COMPILER_ID:MSVC>>:-Wall -Wextra -pedantic -Werror -Wno-unknown-pragmas>)

    # sanitizers
    enable_sanitizers(lapis_lib_${lib_name})
endmacro()
