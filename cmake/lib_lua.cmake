
option(BUILD_LAPIS_LIB_LUA ON)

# library for lua scripting and pipeline format
if(${BUILD_LAPIS_LIB_LUA})
	# dependencies
	lapis_dependency(Lua)
	
    set(lapis_lib_lua_source_files
        "source/lapis_lib_lua/library.cpp"
        "source/lapis_lib_lua/adapter_script_lua.cpp"
        "source/lapis_lib_lua/lua_exception.cpp"
        "source/lapis_lib_lua/lua_function.cpp"
        "source/lapis_lib_lua/lua_script.cpp"
        "source/lapis_lib_lua/lua_util.cpp"
    )
    file(GLOB_RECURSE lapis_lib_lua_private_headers "include/private/lapis_lib_lua/*.hpp")
    add_lapis_library(lua ${lapis_lib_lua_source_files} ${lapis_lib_lua_private_headers})
    target_link_libraries(lapis_lib_lua PRIVATE ${LUA_LIBRARIES})
endif()
