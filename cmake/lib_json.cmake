
option(BUILD_LAPIS_LIB_JSON ON)

# library for json im-/export
if(${BUILD_LAPIS_LIB_JSON})
    set(lapis_lib_json_source_files
        "source/lapis_lib_json/library.cpp"
        "source/lapis_lib_json/interface_json.cpp"
        "source/lapis_lib_json/value_json.cpp"
        "source/lapis_lib_json/tools_json.cpp"
    )
    file(GLOB_RECURSE lapis_lib_json_private_headers "include/private/lapis_lib_json/*.hpp")
    add_lapis_library(json ${lapis_lib_json_source_files} ${lapis_lib_json_private_headers})
endif()
