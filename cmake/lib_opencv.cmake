
option(BUILD_LAPIS_LIB_OPENCV OFF)

# library for image/video manipulation using OpenCV
if(${BUILD_LAPIS_LIB_OPENCV})
    find_package(OpenCV REQUIRED)
    set(lapis_lib_opencv_source_files
        "source/lapis_lib_opencv/library.cpp"
    )
    file(GLOB_RECURSE lapis_lib_opencv_private_headers "include/private/lapis_lib_opencv/*.hpp")
    add_lapis_library(opencv ${lapis_lib_opencv_source_files} ${lapis_lib_opencv_private_headers})
    target_include_directories(lapis_lib_opencv PRIVATE ${OpenCV_INCLUDE_DIRS})
    target_link_libraries(lapis_lib_opencv PRIVATE ${OpenCV_LIBS})
endif()
