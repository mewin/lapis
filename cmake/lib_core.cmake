
option(BUILD_LAPIS_LIB_CORE ON)

# core library
if(${BUILD_LAPIS_LIB_CORE})
    set(lapis_lib_core_source_files
        "source/lapis_lib_core/library.cpp"
        "source/lapis_lib_core/adapter_pipeline_script.cpp"
        "source/lapis_lib_core/tools_bitmap_io.cpp"
        "source/lapis_lib_core/tools_file_io.cpp"
        "source/lapis_lib_core/tools_pipeline_arguments.cpp"
    )
    file(GLOB_RECURSE lapis_lib_core_private_headers "include/private/lapis_lib_core/*.hpp")
    add_lapis_library(core ${lapis_lib_core_source_files} ${lapis_lib_core_private_headers})
endif()
