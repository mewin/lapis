
option(BUILD_LAPIS_LIB_BITMAP_IO ON)

# core library
if(${BUILD_LAPIS_LIB_CORE})
	# dependencies
	lapis_dependency(PNG)
	
    set(lapis_lib_bitmap_io_source_files
        "source/lapis_lib_bitmap_io/library.cpp"
        "source/lapis_lib_bitmap_io/adapter_bitmap_png.cpp"
    )
    file(GLOB_RECURSE lapis_lib_bitmap_io_private_headers "include/private/lapis_lib_bitmap_io/*.hpp")
    add_lapis_library(bitmap_io ${lapis_lib_bitmap_io_source_files} ${lapis_lib_bitmap_io_private_headers})
    target_link_libraries(lapis_lib_bitmap_io PRIVATE ${PNG_LIBRARIES})
endif()
