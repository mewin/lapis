
option(BUILD_DEPENDENCIES OFF)

if(${BUILD_DEPENDENCIES})
	include(cmake/HunterGate.cmake)

	HunterGate(
		URL "https://github.com/cpp-pm/hunter/archive/v0.23.271.tar.gz"
		SHA1 "2d6686024529b471e0cc3a4fc07d4d64b62a53ee"
	)
	macro(lapis_dependency dependency)
		hunter_add_package(${dependency})
		find_package(${dependency} CONFIG REQUIRED)
	endmacro()

	set(FMT_LIBRARIES fmt::fmt)
	set(LUA_LIBRARIES Lua::lua_lib)
	set(PNG_LIBRARIES PNG::PNG)
else()
	macro(lapis_dependency dependency)
		find_package(${dependency} REQUIRED)
	endmacro()

	set(FMT_LIBRARIES fmt)
endif()
