from conans import ConanFile, CMake

class LapisConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "libpng/1.6.37", "fmt/7.0.3", "libjpeg/9d", "lua/5.3.5"
    generators = "cmake"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
